//
// IA.hpp for  in /home/avazas_o/rendu/cpp_bomberman
//
// Made by oleksandr avazashvili
// Login   <avazas_o@epitech.net>
//
// Started on  Mon May  4 17:24:31 2015 oleksandr avazashvili
// Last update Mon May  4 17:24:32 2015 oleksandr avazashvili
//

#ifndef IA_HPP_
# define IA_HPP_

#include <iostream>
#include <pthread.h>
#include <list>
#include <unistd.h>
#include "Thread.hh"
#include "Player.hh"

class Bomberman;

enum difficulty
{
	EASY,
	MEDIUM,
	HARD
};

enum move
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
	NOWHERE
};

enum state
{
	SAFTY,
	DANGER
};

enum obj_type
{
	TYPE_PLAYER = 0,
	TYPE_BOMB,
	TYPE_WALL,
	TYPE_BONUS,
	NOTHING
};

class IA : public Player
{
public:
	IA(int posX, int posY, std::string name, enum difficulty diff, int id, Bomberman *boss);
	IA(int posX, int posY, std::string name, enum difficulty diff, int id, Bomberman *boss, int range, int capacity, int score);
	~IA();

	void				move(enum move where);
	void				setState(enum state newstate);
	void				checkState();
	static void			*getNearestObj(void *obj);
	enum obj_type		checkPosition(int x, int y);
	enum difficulty		getDifficulty() const;
	void				setAround(int n, enum obj_type obj);
	enum move			checkSafe();
	bool				checkifPlayer();
	void				moveSomewhere();
	enum state 			getState() const;
	int					myToint(float num);
	enum move		getDirection() const;

private:
  enum difficulty	_diff;
  enum state		_state;
  Bomberman 		*_bomberman;
  enum obj_type		_around[4];
	int							_i;
	float						_newPosX;
	float						_newPosY;
	enum move				_direction;

};

#endif
