//
// Character.hpp for Character in /home/dayrie_l/rendu/cpp_bomberman/graphic
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon May 11 13:59:15 2015 Lucas Dayries
// Last update Sat Jun 13 14:20:18 2015 Steeven Brunner
//

#pragma once

#include "ACharacter.hpp"
#include "IA.hh"

class Bomberman;

class Character : public ACharacter
{
private:
  gdl::Model	_model;
  int _anim;
  float		_speed;
  bool		_state;
  bool		_button;
  Player * _player;
  glm::vec4 _color;

public:
  Character(Player *player);
  virtual ~Character();

  virtual bool  initialize();
  virtual void  update(gdl::Clock const &clock, gdl::Input &input);
  virtual void  draw(gdl::BasicShader &shader, gdl::Clock const &clock);
  void          moveCharacter(Bomberman * bom, gdl::Clock const &clock, move dir, float speed);
  glm::vec4     getColor() const;
  void          setColor(glm::vec4 color);
  Player *      getPlayer();
};
