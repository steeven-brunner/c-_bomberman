//
// BombGraph.hpp for BombGraph in /home/dayrie_l/rendu/cpp_bomberman/inc
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon Jun  8 11:21:22 2015 Lucas Dayries
// Last update Mon Jun  8 11:21:22 2015 Lucas Dayries
//

#pragma once

#include "AObject.hpp"
#include "Texture.hpp"

class BombGraph : public AObject
{
private:
  gdl::Texture	*_texture;
  gdl::Geometry	_geometry;
  float		_speed;
  gdl::Model _model;
  bool       _drop;
  float      _resize;
  int        _i;

public:
  BombGraph();
  BombGraph(Texture *tex);
  virtual ~BombGraph();

  virtual bool	initialize();
  virtual void	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::BasicShader &shader, gdl::Clock const &clock);

  void setDrop(bool set);

  void reset();
};
