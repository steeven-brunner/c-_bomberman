//
// Explosion.hh for bomberman in /home/ganter_v/rendu/cpp_bomberman/inc/
//
// Made by Victor GANTER
// Login   <ganter_v@epitech.eu>
//
// Started on  Wed Jun 10 15:47:41 2015 Victor GANTER
// Last update Sat Jun 13 14:40:18 2015 Steeven Brunner
//

#ifndef EXPLOSION_HH_
#define EXPLOSION_HH_

#include <chrono>

#include "AObject.hpp"
#include "Texture.hpp"
#include "Wall.hh"

class Explosion : public AObject
{
public:
  Explosion();
  Explosion(Texture *tex);
  virtual ~Explosion();

  virtual bool	initialize();
  virtual void	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::BasicShader &shader, gdl::Clock const &clock);
  void checkDestroy();
  bool getPrint() const;
  void Activate();

private:
  gdl::Texture	*_texture;
  gdl::Geometry	_geometry;
  float		_speed;
  std::chrono::steady_clock::time_point _timestart;
  bool    _print;
};

#endif /* !EXPLOSION_HH_ */
