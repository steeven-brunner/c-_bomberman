//
// GameEngine.hpp for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/inc/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Sat Jun 13 14:03:39 2015 Victor Ganter
// Last update Sun Jun 14 17:24:45 2015 Edwin SCHLERET
//

#pragma once

#include <Game.hh>
#include <SdlContext.hh>
#include "AObject.hpp"
#include "SkyMap.hpp"
#include "BombGraph.hpp"
#include "Texture.hpp"
#include "ACharacter.hpp"
#include "Cube.hpp"
#include "SolidCube.hpp"
#include "Floor.hpp"
#include "Character.hpp"
#include "Explosion.hh"
#include "BonusGraph.hh"
#include "IA.hh"

class Bomberman;

class GameEngine : public gdl::Game
{
public:
  GameEngine(Bomberman *bomb, bool multi, int x, int y);
  ~GameEngine();

  bool initialize();
  bool update();
  void draw();
  void moveCharacter(int id, move dir, float speed = 0);
  void	setMulti(bool multi);

  std::vector<AObject*> *getBombs();
  std::vector<Explosion*> *getExplosions();
  std::vector<BonusGraph*> *getBonus();
  gdl::Input *getInput();
  gdl::SdlContext &getContext();

  int getBombNb();
  void incrBombNb();
  int getBombNbErase();
  void incrBombNbErase();

  glm::mat4 _projection;
  glm::mat4 _transformation;

  void dropBomb(float x, float y);

private:
  void  drawGame();

private:
  Bomberman *_bomb;
  gdl::SdlContext _context;
  gdl::Clock _clock;
  gdl::Input _input;
  gdl::BasicShader _shader;
  AObject *_skymap;
  std::vector<AObject*> _walls;
  std::vector<AObject*> _floors;
  std::vector<ACharacter*> _characters;
  std::vector<AObject*> _bombs;
  int bombNb;
  int bombNbErase;
  std::vector<Explosion*> _explosions;
  std::vector<BonusGraph*> _bonus;
  int _x;
  int _y;
  int _z;
  bool	_multi;
  int	_sizeX;
  int	_sizeY;
  bool  _p1Alive;
  bool  _p2Alive;
};
