//
// HardObject.hh for HardObject.hh in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 14:57:40 2015 Steeven Brunner
// Last update Mon May  4 16:24:01 2015 Steeven Brunner
//

#ifndef HARDOBJECT_HH_
# define HARDOBJECT_HH_

#include <iostream>
#include "Object.hh"

class	HardObject : public Object
{
public:
  HardObject(float posX, float posY);
  ~HardObject();
};

#endif /* !HARDOBJECT_HH_ */
