//
// IMutex.hh for cpp_plazza in /home/schler_e/Projet Epitech/C++/cpp_plazza/tp/
//
// Made by Edwin SCHLERET
// Login   <schler_e@epitech.eu>
//
// Started on  Wed Apr 15 11:42:51 2015 Edwin SCHLERET
// Last update Mon May 11 10:41:14 2015 Edwin SCHLERET
//

#ifndef MUTEX_HH_
# define MUTEX_HH_

# include <iostream>
# include <pthread.h>

# include "Error.hh"

class Mutex
{
public:
  Mutex();
  ~Mutex();
  int               lock();
  int               unlock();
  bool              trylock();
  pthread_mutex_t   *getMutex();

private:
  pthread_mutex_t   _mutex;
};

#endif /* !MUTEX_HH_ */
