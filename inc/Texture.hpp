//
// Texture.hpp for Texture in /home/dayrie_l/rendu/cpp_bomberman/src
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Fri May 22 13:57:08 2015 Lucas Dayries
// Last update Fri May 22 13:57:09 2015 Lucas Dayries
//

#pragma once

#include "AObject.hpp"

class Texture
{
public:
	Texture();
	~Texture();

	bool initialize();
	void assignTexture(gdl::Texture *texture, std::string texName);
	gdl::Texture *getTexWall();
	gdl::Texture *getTexSolid();
	gdl::Texture *getTexFloor();
	gdl::Texture *getTexSkyMap();
	gdl::Texture *getTexExplosion();
	gdl::Texture *getTexBomb();
	gdl::Texture *getTexBonusFire();
	gdl::Texture *getTexBonusBomb();
	gdl::Texture *getTexBonusSpeed();
	bool tex;

private:
	gdl::Texture _textureWall;
	gdl::Texture _textureSolid;
	gdl::Texture _textureFloor;
	gdl::Texture _textureSkyMap;
	gdl::Texture _textureExplosion;
	gdl::Texture _textureBomb;
	gdl::Texture _textureBonusFire;
	gdl::Texture _textureBonusBomb;
	gdl::Texture _textureBonusSpeed;
};

typedef void (Texture::*textures)(gdl::Texture *texture, std::string texName);
