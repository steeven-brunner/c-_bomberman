//
// SoftObject.hh for Bomberman in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 15:10:16 2015 Steeven Brunner
// Last update Mon May  4 16:23:50 2015 Steeven Brunner
//

#ifndef SOFTOBJECT_HH_
# define SOFTOBJECT_HH_

# include <iostream>
# include "Object.hh"

class	SoftObject : public Object
{
public:
  SoftObject(float posX, float posY);
  ~SoftObject();
};

#endif /* !SOFTOBJECT_HH_ */
