//
// ScopedLock.hh for cpp_plazza in /home/schler_e/Projet Epitech/C++/cpp_plazza/tp/
//
// Made by Edwin SCHLERET
// Login   <schler_e@epitech.eu>
//
// Started on  Thu Apr 16 14:46:51 2015 Edwin SCHLERET
// Last update Fri Apr 17 10:01:44 2015 Edwin SCHLERET
//

#ifndef SCOPEDLOCK_HH_
# define SCOPEDLOCK_HH_

# include <pthread.h>

# include "Mutex.hh"

class ScopedLock
{

public:

  ScopedLock(Mutex *mutex) :
    _mutex(mutex)
  {
    _mutex->lock();
  }

    ~ScopedLock(void)
  {
    _mutex->unlock();
  }

private:
    ScopedLock(ScopedLock const&);
    ScopedLock& operator=(ScopedLock const&);

    Mutex   *_mutex;
};

#endif /* !SCOPEDLOCK_HH_ */
