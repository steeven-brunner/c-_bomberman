//
// Error.hh for Error in /home/gaessl_m/rendu/cpp_plazza/Error
//
// Made by Marin Gaessler
// Login   <gaessl_m@epitech.net>
//
// Started on  Thu Apr 23 13:03:44 2015 Marin Gaessler
// Last update Thu Apr 23 14:44:41 2015 Marin Gaessler
//

#ifndef ERROR_HH_
# define ERROR_HH_

#include <stdexcept>
#include <string>

class	Error : public std::exception
{
public:
  Error(std::string const &message, std::string const &component);
  ~Error() throw();
  const char*	getComponent() const;
  const char*	what() const throw();

private:
  std::string	_message;
  std::string	_component;
};

class	ThreadError : public Error
{
public:
  ThreadError(std::string const &message, std::string const &component = "ThreadError");
  ~ThreadError() throw();
};

class	SystemError : public Error
{
public:
  SystemError(std::string const &message, std::string const &component = "SystemError");
  ~SystemError() throw();
};

class	GraphicError : public Error
{
public:
  GraphicError(std::string const &message, std::string const &component = "GraphicError");
  ~GraphicError() throw();
};

class	MapError : public Error
{
public:
  MapError(std::string const &message, std::string const &component = "MapError");
  ~MapError() throw();
};

#endif /* !ERROR_HH_ */
