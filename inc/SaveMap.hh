//
// SaveMap.hh for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/inc/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Thu May 21 14:26:17 2015 Victor Ganter
// Last update Sun Jun 14 16:33:05 2015 Edwin SCHLERET
//

#ifndef SAVEMAP_HH_
# define SAVEMAP_HH_

# include <sstream>
# include <ostream>
# include <fstream>
# include <ctime>

# include "Bomberman.hh"
# include "IA.hh"

class SaveMap
{
public:
  SaveMap (Bomberman &bomberman, std::string);
  ~SaveMap ();
  int writeWalls(std::list<Wall> *, std::ofstream&);
  int writePlayers(std::list<Player> *, std::list<IA> *, std::ofstream&);
  int writeBomb(std::list<Bomb> *, std::ofstream&);
  int writeBonus(std::list<Bonus> *, std::ofstream&);
  static std::string      getSaveName();
};

#endif /* !SAVEMAP_HH_ */
