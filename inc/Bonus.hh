//
// Bonus.hh for Bonus.hh in /home/brunne_s/rendu/cpp_bomberman/Game
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Tue May  5 17:22:02 2015 Steeven Brunner
// Last update Tue May  5 18:19:55 2015 Steeven Brunner
//

#ifndef BONUS_HH_
# define BONUS_HH_

#include <iostream>
#include "SoftObject.hh"

enum	BonusType
{
  MORE_BOMB = 0,
  FASTER,
  MORE_RANGE,
  SIZE_BONUS
};

class	Bonus : public SoftObject
{
public:
  Bonus(float posX, float posY, BonusType type);
  ~Bonus();
  BonusType	getType() const;
  bool      isShow() const;
  void      setIsShow(bool show);

private:
  BonusType	_type;
  bool      _isShow;
};

#endif /* !BONUS_HH_ */
