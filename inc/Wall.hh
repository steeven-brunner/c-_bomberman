//
// Wall.hh for Bomberman in /home/brunne_s/rendu/cpp_bomberman/Game
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Wed May  6 11:25:51 2015 Steeven Brunner
// Last update Mon May 18 15:56:41 2015 Steeven Brunner
//

#ifndef WALL_HH_
# define WALL_HH_

#include <iostream>
#include "HardObject.hh"
#include "BonusGraph.hh"

class Bomberman;

class	Wall : public HardObject
{
public:
  Wall(float posX, float posY, Bomberman * bomberman, bool isBreakable = true);
  ~Wall();
  void	setIsBreakable(bool isBreakable);
  bool	getIsBreakable() const;
  bool  isShow() const;
  void  setShow(bool show);
  void  breakWall(std::vector<BonusGraph*> *bonus);

private:
  bool	     _isBreakable;
  bool	     _isShow;
  Bomberman* _bomberman;
};

#endif /* !WALL_HH_ */
