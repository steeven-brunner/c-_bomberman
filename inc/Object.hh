//
// Object.hh for Bomberman in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 14:56:31 2015 Steeven Brunner
// Last update Wed May  6 17:19:31 2015 Steeven Brunner
//

#ifndef OBJECT_HH_
# define OBJECT_HH_

#include <iostream>

class	Object
{
public:
  Object(float posX, float posY, bool _isSolid);
  ~Object();
  float	getPosX() const;
  float	getPosY() const;
  void	setPosX(float posX);
  void	setPosY(float posY);
  bool	getType() const;
  bool	getIsSolid() const;
  bool	checkCollision(float x, float y, Object & obj) const;

protected:
  float	_posX;
  float	_posY;
  bool	_isSolid;
};

#endif /* !OBJECT_HH_ */
