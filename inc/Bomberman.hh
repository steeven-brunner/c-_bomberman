//
// Bomberman.hh for bomberman in /home/schler_e/Projet Epitech/C++/cpp_bomberman/
//
// Made by Edwin SCHLERET
// Login   <schler_e@epitech.eu>
//
// Started on  Tue May  5 10:13:41 2015 Edwin SCHLERET
// Last update Sun Jun 14 16:38:52 2015 Edwin SCHLERET
//

#ifndef BOMBERMAN_HH_
# define BOMBERMAN_HH_

#include <list>
#include <iterator>
#include <tuple>

#include "Player.hh"
#include "Bonus.hh"
#include "Bomb.hh"
#include "Wall.hh"
#include "IA.hh"
#include "Thread.hh"
#include "CondVar.hh"
#include "Mutex.hh"
#include "ScopedLock.hh"
#include "GameEngine.hpp"
#include "BombGraph.hpp"
#include "Score.hh"
#include "GameOver.hh"

class     SaveMap;

struct    structPlayer
{
  int         id;
  std::string name;
};

struct    structIA
{
  int         id;
  std::string name;
  enum difficulty  difficulty;
};

struct    keybind
{
  int     up;
  int     down;
  int     left;
  int     right;
  int     bomb;
};

class Bomberman
{
public:
  Bomberman(std::string namesave, int x, int y);
  Bomberman(std::string map, std::list<structPlayer> & player, std::list<structIA> & ia, int x, int y);
  Bomberman(int sizeX, int sizeY, std::list<structPlayer> & player, std::list<structIA> & ia, int x, int y);
  ~Bomberman();

  void                setSizeX(int x);
  void                setSizeY(int y);
  int                 getSizeX() const;
  int                 getSizeY() const;
  int                 getNbrPlayer() const;
  int                 getNbrHuman() const;
  int                 getNbrIA() const;
  std::list<Player> * getPlayer();
  std::list<IA> *     getIA();
  std::list<Bonus> *  getBonus();
  std::list<Bomb> *   getBomb();
  std::list<Wall> *   getWall();
  GameEngine *        getDisplay();
  Mutex *             getMutex();

  void                IALoop(int id, IA * ia);
  void                playerLoop(int id, Player * player);
  void                gameLoop();
  void                startGame();
  void                CheckEnd();

private:
  static void *       IALoopFunc(void *data);
  static void *       playerLoopFunc(void *data);
  void                checkAction(keybind & key, Player * player);

private:
  int                   _sizeX;
  int                   _sizeY;
  int                   _x;
  int                   _y;
  int                   _nbrPlayer;
  int                   _nbrPerso;
  bool                  _end;
  bool                  _pause;
  Mutex                 _mutex;
  CondVar               _condVar;
  keybind               _p1;
  keybind               _p2;
  gdl::Input*           _input;
  GameEngine            _display;
  Score                 _score;
  std::string           _nameGame;

  std::list<Player>     _player;
  std::list<IA>         _ia;
  std::list<Bonus>      _bonus;
  std::list<Bomb>       _bomb;
  std::list<Wall>       _wall;
  std::list<Thread>     _thread;
};

#endif /* !BOMBERMAN_HH_ */
