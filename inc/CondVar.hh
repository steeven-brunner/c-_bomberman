//
// CondVar.hh for CondVar in /home/gaessl_m/rendu/cpp_plazza/Kitchen
//
// Made by Marin Gaessler
// Login   <gaessl_m@epitech.net>
//
// Started on  Mon Apr 20 16:00:26 2015 Marin Gaessler
// Last update Mon Apr 20 16:09:23 2015 Marin Gaessler
//

#ifndef CONDVAR_HH_
# define CONDVAR_HH_

# include <pthread.h>
# include <iostream>

# include "Mutex.hh"
# include "Error.hh"

class	CondVar
{
public:
  CondVar();
  CondVar(Mutex *mutex);
  ~CondVar();
  void  init(Mutex *mutex);
  void	wait();
  void	signal();
  void	broadcast();

private:
  pthread_cond_t	_cond;
  Mutex	          *_mutex;
};

#endif /* !CONDVAR_HH_ */
