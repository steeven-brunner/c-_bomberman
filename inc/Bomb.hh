//
// Bomb.hh for Bomb.hh in /home/brunne_s/rendu/cpp_bomberman/Game
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Tue May  5 17:09:52 2015 Steeven Brunner
// Last update Thu Jun 11 16:09:14 2015 Steeven Brunner
//

#ifndef BOMB_HH_
# define BOMB_HH_

#include <iostream>
#include <chrono>
#include "Bomberman.hh"
#include "IA.hh"
#include "Player.hh"
#include "Wall.hh"
#include "SoftObject.hh"
#include "AObject.hpp"
#include "Explosion.hh"

class	Bomb : public SoftObject
{
public:
  Bomb(float posX, float posY, int range, int playerId, int time = 3);
  ~Bomb();
  int	DestroyObj(Bomberman &bomberman, int posX, int posY,
		   bool isPlayer, Player &itPlayer, IA &itIA);
  void	checkBombList(Bomberman &bomberman);
  bool checkWall(int posX, int posY, Bomberman &bomberman);
  bool addEffect(std::vector<Explosion*> *explosions, std::list<Bomb>::iterator itBomb, Bomberman &bomberman);
  int	getRange() const;
  int	getPlayerId() const;
  int	getTime() const;
  std::chrono::steady_clock::time_point	getStartTime() const;
private:
  int					_range;
  int					_playerId;
  int					_time;
  std::chrono::steady_clock::time_point _startTime;
  bool					_willExplose;
};

#endif /* !BOMB_HH_ */
