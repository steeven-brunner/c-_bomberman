//
// Score.hh for Score.hh in /home/brunne_s/rendu/cpp_bomberman/inc
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Thu May 21 17:23:25 2015 Steeven Brunner
// Last update Mon Jun  8 12:48:20 2015 Steeven Brunner
//

#ifndef SCORE_HH_
# define SCORE_HH_

#include <fstream>
#include <tuple>
#include <list>
#include <iostream>

class Bomberman;

class	Score
{
public:
  Score();
  ~Score();
  void                                        	saveScore(Bomberman & bomberman);
  std::list<std::tuple<std::string, int> >    	loadScore();
  std::list<std::tuple<std::string, int> >		getTop(std::list<std::tuple<std::string, int> > list, size_t listSize);
};

#endif /* !SCORE_HH_ */
