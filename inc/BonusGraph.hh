//
// BonusGraph.hh for bomberman in /home/ganter_v/rendu/cpp_bomberman/inc/
//
// Made by Victor GANTER
// Login   <ganter_v@epitech.eu>
//
// Started on  Thu Jun 11 14:48:30 2015 Victor GANTER
// Last update Sun Jun 14 15:12:11 2015 Victor Ganter
//

#pragma once

#include "AObject.hpp"
#include "Bonus.hh"

class BonusGraph : public AObject
{
public:
  BonusGraph(gdl::Texture	*bomb, gdl::Texture	*fire, gdl::Texture	*speed);
  virtual ~BonusGraph();

  virtual bool	initialize();
  virtual void	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::BasicShader &shader, gdl::Clock const &clock);
  void  setActive(Bonus *, int x, int y, int typebonus);
  void  unActive();
  bool  checkActive();
  Bonus *getBonus() const;
  glm::vec3 getPos();


private:
  gdl::Texture	*_texturebomb;
  gdl::Texture	*_texturefire;
  gdl::Texture	*_texturespeed;
  gdl::Geometry	_geometry;
  int           _type;
  bool _active;
  Bonus *_bonus;
  int           _i;
  float         _vitesse;
};
