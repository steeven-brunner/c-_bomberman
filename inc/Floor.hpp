//
// Floor.hpp for Floor in /home/dayrie_l/rendu/cpp_bomberman/graphic
// 
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
// 
// Started on  Tue May 12 16:09:05 2015 Lucas Dayries
// Last update Tue May 12 16:27:24 2015 Lucas Dayries
//

#pragma once

#include "AObject.hpp"
#include "Texture.hpp"

class Floor : public AObject
{
private:
  gdl::Texture	*_texture;
  gdl::Geometry	_geometry;
  float		_speed;

public:
  Floor();
  Floor(Texture *tex);
  virtual ~Floor();

  virtual bool	initialize();
  virtual void	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::BasicShader &shader, gdl::Clock const &clock);
};
