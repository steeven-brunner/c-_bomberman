//
// GenerateMap.hh for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/Game/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Thu May  7 11:19:36 2015 Victor Ganter
// Last update Fri May 22 13:57:05 2015 Victor Ganter
//

#ifndef GENERATEMAP_HH_
# define GENERATEMAP_HH_

#include <iostream>
#include <list>
#include <vector>
#include <fstream>

#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "Bomberman.hh"
#include "Player.hh"
#include "Wall.hh"
#include "Bomb.hh"
#include "Bonus.hh"
#include "IA.hh"

#define HOR 1
#define VER 2

typedef struct s_pospos
{
  int           x;
  int           y;
}              t_pospos;

class GenerateMap
{
public:
  GenerateMap(Bomberman *bomberman);
  GenerateMap(Bomberman *bomberman, std::list<structPlayer> player, std::list<structIA> ia);
  ~GenerateMap();

  void add_cassable(int x, int y);
  void add_incassable(int x, int y);
  void add_player(int x, int y, std::string name, int id);
  void add_player(int x, int y, std::string name, int id, int range, int capacity, int score);
  void add_IA(int x, int y, std::string name, difficulty diff, int id);
  void add_IA(int x, int y, std::string name, difficulty diff, int id, int range, int capacity, int score);
  void add_Bonus(int x, int y, BonusType type);
  void add_Bomb(int x, int y, int range, int playerId, int time);
  std::vector<t_pospos> generatepospos(int length, int height);
  int placeplayers(std::vector<t_pospos> listpos, std::list<structPlayer> player, std::list<structIA> ia);
  int checknearplayer(int xbis, int ybis, int dir);
  int searchnearplayer(int x, int y, int dir);
  int randwall(int x, int y);
  int generate(int x, int y, std::list<structPlayer> player, std::list<structIA> ia);

protected:
  std::list<Player> *_Players;
  std::list<IA>   *_IA;
  std::list<Bomb>   *_Bombe;
  std::list<Bonus>  *_Bonus;
  std::list<Wall>   *_Mur;
  Bomberman         *_bomberman;
};

#endif /* !GENERATEMAP_HH_ */
