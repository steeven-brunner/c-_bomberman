//
// Cube.hpp for Cube in /home/dayrie_l/rendu/cpp_bomberman/graphic/test
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Wed May  6 14:10:34 2015 Lucas Dayries
// Last update Sat Jun 13 14:21:28 2015 Steeven Brunner
//

#pragma once

#include "AObject.hpp"
#include "Texture.hpp"
#include "Wall.hh"

class Cube : public AObject
{
private:
  gdl::Texture	*_texture;
  gdl::Geometry	_geometry;
  float		_speed;
  Wall *    _wall;

public:
  Cube();
  Cube(Texture *tex, Wall * wall);
  virtual ~Cube();

  virtual bool	initialize();
  virtual void	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::BasicShader &shader, gdl::Clock const &clock);
  Wall *        getWall();
};
