//
// Player.hh for Bomberman in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 13:20:59 2015 Steeven Brunner
// Last update Wed May 20 17:16:51 2015 Steeven Brunner
//

#ifndef PLAYER_HH_
# define PLAYER_HH_

# include <iostream>
# include <string>

# include "SoftObject.hh"

class Bomberman;

class	Player : public SoftObject
{
public:
  Player(float posX, float posY, std::string name, int id);
  Player(float posX, float posY, std::string name, int id, int range, int capacity, int score);
  ~Player();
  bool          checkObject(Bomberman & bomberman, float posX, float posY);
  bool          movePlayer(Bomberman & bom, float x, float y);
  void          placeBomb(Bomberman & bomberman);
  std::string	  getName() const;
  void	        setName(std::string);
  int           getId() const;
  int           getRange() const;
  int           getCapacity() const;
  void          setRange(int range);
  void          setCapacity(int capacity);
  int           getScore() const;
  void          setScore(int score);
  void          setAlive(bool alive);
  bool          isAlive() const;
  float         getSpeed() const;
  void          setSpeed(float speed);

protected:
  std::string	_name;
  int		_id;
  int		_score;
  int		_range;
  int		_capacity;
  bool  _isAlive;
  float _speed;
};

#endif /* !PLAYER_HH_ */
