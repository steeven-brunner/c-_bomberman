//
// GameOver.hh for bomerman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/inc/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Fri Jun 12 14:36:34 2015 Victor Ganter
// Last update Sun Jun 14 14:25:02 2015 Victor Ganter
//

#pragma once

# include <iostream>
# include <sstream>

# include "SDL2/SDL.h"
# include "SDL2/SDL_image.h"
# include "SDL2/SDL_ttf.h"

class GameOver
{
public:
  GameOver(int x, int y, int);
  ~GameOver();
  std::string inputName();
  void printwin();


private:
  SDL_Window* _window;
  SDL_DisplayMode _current;
  SDL_Texture*		_texture;
  SDL_Renderer*		_renderer;
  SDL_Surface*    _gameover;
  TTF_Font*       _police;
  int             _x;
  int             _y;
  int             _idwin;
};
