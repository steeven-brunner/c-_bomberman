//
// ACharacter.hpp for ACharacter in /home/dayrie_l/rendu/cpp_bomberman/graphic
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon May 11 14:14:54 2015 Lucas Dayries
// Last update Sat Jun 13 13:07:23 2015 Steeven Brunner
//

#pragma once

#include <iostream>
#include <Game.hh>
#include <Clock.hh>
#include <Input.hh>
#include <SdlContext.hh>
#include <Geometry.hh>
#include <Texture.hh>
#include <BasicShader.hh>
#include <Model.hh>
#include <OpenGL.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class ACharacter
{
public:
  ACharacter();
  virtual ~ACharacter();

  virtual bool initialize();
  virtual void update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::BasicShader &shader, gdl::Clock const &clock) = 0;

  void translate(glm::vec3 const &v);
  void rotate(glm::vec3 const& axis, float angle);
  void scale(glm::vec3 const& scale);
  glm::mat4 getTransformation();

  glm::vec3 getPosition();

protected:
  glm::vec3	_position;
  glm::vec3	_rotation;
  glm::vec3	_scale;
};
