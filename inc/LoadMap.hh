//
// LoadMap.hh for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/Game/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Fri May 15 10:43:03 2015 Victor Ganter
// Last update Sat Jun 13 16:34:40 2015 Victor Ganter
//

#ifndef LOADMAP_HH_
# define LOADMAP_HH_

#include <sstream>

#include "GenerateMap.hh"

class LoadMap : public GenerateMap
{
public:
  LoadMap(std::string save, Bomberman *bomberman);
  LoadMap(std::string map, Bomberman *bomberman, std::list<structPlayer> player, std::list<structIA> IA);
  ~LoadMap();

  int ParseBreakable(std::istringstream &words, int line);
  int ParseUnBreakable(std::istringstream &words, int line);
  int ParseBomb(std::istringstream &words, int line);
  int ParseBonus(std::istringstream &words, int line);
  int ParsePlayer(std::istringstream &words, int line);
  int ParseIa(std::istringstream &words, int line);
  int lexCmd(std::string cmd, std::istringstream &, int line);
  int checkspace(int xbis, int ybis, int dir);
  std::vector<t_pospos> searchPosposibility(int x, int y);
  int read_map(std::istream &, Bomberman *bomberman, std::list<structPlayer> player, std::list<structIA> IA);
  int read_save(std::istream &stream, Bomberman *bomberman);

};

#endif /* !LOADMAP_HH_ */
