//
// SolidCube.hpp for SolidCube in /home/dayrie_l/rendu/cpp_bomberman/src
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Wed May 20 14:45:58 2015 Lucas Dayries
// Last update Wed May 20 14:46:04 2015 Lucas Dayries
//

#pragma once

#include "AObject.hpp"
#include "Texture.hpp"
#include "Wall.hh"

class SolidCube : public AObject
{
private:
  gdl::Texture	*_texture;
  gdl::Geometry	_geometry;
  float		_speed;
  Wall *    _wall;

public:
  SolidCube();
  SolidCube(Texture *tex, Wall * wall);
  virtual ~SolidCube();

  virtual bool	initialize();
  virtual void	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::BasicShader &shader, gdl::Clock const &clock);
};
