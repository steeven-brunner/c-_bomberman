//
// Thread.hh for Thread in /home/gaessl_m/rendu/cpp_plazza/tp
//
// Made by Marin Gaessler
// Login   <gaessl_m@epitech.net>
//
// Started on  Thu Apr 16 14:19:04 2015 Marin Gaessler
// Last update Thu Apr 16 15:24:07 2015 Marin Gaessler
//

#ifndef THREAD_HH_
# define THREAD_HH_

# include <pthread.h>
# include <csignal>


# include "Error.hh"

enum Status
  {
    NotCreated,
    Running,
    Dead
  };

typedef void *(*pFct)(void *);

class	Thread
{
public:
  Thread();
  ~Thread();

  int		create(pFct f, void *arg);
  int		join();
  void  kill(int sig);
  Status	getStatus() const;

private:
  pthread_t	_thread;
  Status	_status;
};

#endif /* !THREAD_HH_ */
