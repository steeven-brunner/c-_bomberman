//
// Menu.hpp for Menu in /home/gaessl_m/rendu/cpp_bomberman/graphic/menus
//
// Made by Marin GAESSLER
// Login   <gaessl_m@epitech.net>
//
// Started on  Mon May 11 14:29:21 2015 Marin GAESSLER
// Last update Sun Jun 14 18:01:08 2015 Marin Gaessler
//

#ifndef MENU_HH_
# define MENU_HH_

# include <iostream>
# include <sstream>
# include <array>
# include "SDL2/SDL.h"
# include "SDL2/SDL_image.h"
# include "SDL2/SDL_ttf.h"
# include "SDL2/SDL_mixer.h"
# include "Bomberman.hh"
# include "Score.hh"

static const SDL_Color WHITE = {255, 255, 255, 0};
static const SDL_Color BLACK = {20, 0, 0, 0};
static const SDL_Color GREY = {150, 150, 150, 0};
static const int MAX_IA = 2;

struct Resolution
{
	int		x;
	int		y;
};

struct structSave
{
	bool	empty;
	int 	id;
	int 	nbPlayers;
	int 	nbIAs;
	int 	nbPlayersAlive;
	int 	nbIAsAlive;
};

struct structMap
{
	std::string	mapName;
	std::string	fileName;
};

enum SkillIA { IAEASY = 0, IAMEDIUM, IAHARD, SIZE };

static const char * const Skills[SIZE] = {"EASY", "MEDIUM", "HARD"};

class Menu
{
public:
	Menu(int x, int y);
	~Menu();

	void	intro();
	int	init();
	void	initSaves();
	void	fillSave(std::string saveName, int id);
	void 	initMaps();
	void	fillMaps(std::string fileName);
	void	startGame();
	void	startGameCustomMap();
	void	displayTextInMenu(SDL_Color color, const char *str, int x, int y, int w, int h);

	void	initWindow();
	void	destroyWindow();
	void	changeResolution(Resolution r);

	void	menuSettings(int nbPlayers);
	void	initRenderSettings();
	void	displaySettings(int cursorY);
	void 	initIA(int id);
	void	inputSettings(int cursorY, int opt);

	void	settingsLoadedMap();
	void 	initSettingsLoadedMap();
	void 	displaySettingsLoadedMap(int cursor);
	void 	inputLoadedMap(int cursor, int opt);

	void	initMapsAndSaves();
	void	displayMapsAndSaves(int cursor);
	void	loadMapsAndSaves();

	void	loadSave(int cursor);
	bool	checkSave(int cursor);

	void 	initGameLoad();
	void 	displayGameLoad(int cursor);
	void	displaySaves(struct structSave save, int cpt);
	void 	fillGameLoad(int line, int column);
	void	loadGame();

	void	menuOptions();
	void 	initRenderOptions();
	void 	displayResolutionOptions(int i);

	void	initRankings();
	void	displayTopRankings(std::list<std::tuple<std::string, int> >  list);
	void 	displayRankings();

	void	quit();

	void	mainMenu();
	void	displayMainMenu();

	void	swapMenu(int);

private:
	int									_x;
	int									_y;
	int 								_cursorSpot;
	bool								_end;
	int 								_inputMapX;
	int 								_inputMapY;
	int 								_inputNbIA;
	int									_inputSkillIA;
	int 								_nbPlayers;
	size_t 								_scoreSize;
	size_t 								_mapCpt;
	size_t 								_modeCpt;
	std::array<struct Resolution, 4> 	_resolution;
	std::list<structPlayer> 			_player;
  	std::list<structIA> 				_ia;
  	std::vector<structSave>				_saves;
  	std::list<structMap>				_maps;
  	std::list<std::string>				_modes;
	SDL_Window*							_window;
	SDL_Texture*						_texture;
	SDL_Renderer*						_renderer;
	SDL_DisplayMode						_current;
	TTF_Font*							_police;
	SDL_Surface*						_surfaceMsg;
	SDL_Surface*						_cursor;
	SDL_Surface*						_surface;
	SDL_Rect							_rSize;
	SDL_Rect							_pxl;
	SDL_Event							_e;
	Mix_Music 						*_music;
};

#endif /* !MENU_HH_ */
