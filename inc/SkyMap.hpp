//
// SkyMap.hpp for SkyMap in /home/dayrie_l/rendu/cpp_bomberman
// 
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
// 
// Started on  Mon Jun  8 17:16:03 2015 Lucas Dayries
// Last update Mon Jun  8 17:16:04 2015 Lucas Dayries
//

#pragma once

#include "AObject.hpp"
#include "Texture.hpp"

class SkyMap : public AObject
{
private:
  gdl::Texture	*_texture;
  gdl::Geometry	_geometry;
  float		_speed;

public:
  SkyMap();
  SkyMap(Texture *tex);
  virtual ~SkyMap();

  virtual bool	initialize();
  virtual void	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::BasicShader &shader, gdl::Clock const &clock);
};