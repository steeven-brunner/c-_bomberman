##
## Makefile for bomberman in /home/schler_e/Projet Epitech/C++/cpp_bomberman/Game/
##
## Made by Edwin SCHLERET
## Login   <schler_e@epitech.eu>
##
## Started on  Thu May  7 10:22:37 2015 Edwin SCHLERET
## Last update Sun Jun 14 16:41:17 2015 Edwin SCHLERET
##

CXX			= g++

RM   		= rm -rf

NAME  		= bomberman

SRCS_DIR	= src

OBJS_DIR	= obj

SRCS  		=	ACharacter.cpp \
				AObject.cpp \
				Bomb.cpp \
				BombGraph.cpp \
				Bomberman.cpp \
				Bonus.cpp \
				Character.cpp \
				CondVar.cpp \
				Cube.cpp \
				Error.cpp \
				Floor.cpp \
				GameEngine.cpp \
				GenerateMap.cpp \
				HardObject.cpp \
				IA.cpp \
				LoadMap.cpp \
				main.cpp \
				Menu.cpp \
				Mutex.cpp \
				Object.cpp \
				Player.cpp \
				Score.cpp \
				SoftObject.cpp \
				Thread.cpp \
				Texture.cpp \
				Wall.cpp \
				SolidCube.cpp \
				SkyMap.cpp \
				Explosion.cpp \
				BonusGraph.cpp \
				GameOver.cpp \
				SaveMap.cpp

OBJS = $(patsubst %,$(OBJS_DIR)/%,$(SRCS:.cpp=.o))

CXXFLAGS  = -Iinc -Ilib_graphic/includes/ -W -Wall -Wextra -Werror -std=c++11

LDFLAGS		= -Llib_graphic/libs/ -lgdl_gl -lGLEW -lGL -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -ldl -lrt -lfbxsdk -lpthread

$(OBJS_DIR)/%.o: $(SRCS_DIR)/%.cpp
	@[ -d $(OBJS_DIR) ] || mkdir -p $(OBJS_DIR)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

all: $(NAME)

$(NAME): $(OBJS)
	$(CXX) -o $(NAME) $(OBJS) $(LDFLAGS)

debug: CXXFLAGS += -g
debug: all

clang: CXX = clang++
clang: all

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(OBJS_DIR)
	$(RM) $(NAME)

re: fclean all
