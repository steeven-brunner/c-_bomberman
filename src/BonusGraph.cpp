//
// BonusGraph.cpp for bobmerman in /home/ganter_v/rendu/cpp_bomberman/src/
//
// Made by Victor GANTER
// Login   <ganter_v@epitech.eu>
//
// Started on  Thu Jun 11 14:48:11 2015 Victor GANTER
// Last update Sun Jun 14 15:18:46 2015 Victor Ganter
//

#include "BonusGraph.hh"

BonusGraph::BonusGraph(gdl::Texture	*bomb, gdl::Texture	*fire, gdl::Texture	*speed) : _i(0), _vitesse(0.005)
{
  _position = glm::vec3(0, 0, 0);
  _active = false;
  _texturebomb = bomb;
  _texturefire = fire;
  _texturespeed = speed;
}

BonusGraph::~BonusGraph()
{
}

bool	BonusGraph::initialize()
{
  _geometry.pushVertex(glm::vec3(0.5, -0.5, 0.5));
  _geometry.pushVertex(glm::vec3(0.5, 0.5, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5, 0.5, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, 0.5));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.pushVertex(glm::vec3(0.5, -0.5, -0.5));
  _geometry.pushVertex(glm::vec3(0.5, 0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, 0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, -0.5));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.pushVertex(glm::vec3(0.5, -0.5, -0.5));
  _geometry.pushVertex(glm::vec3(0.5, 0.5, -0.5));
  _geometry.pushVertex(glm::vec3(0.5, 0.5, 0.5));
  _geometry.pushVertex(glm::vec3(0.5, -0.5, 0.5));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5, 0.5, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5, 0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, -0.5));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.pushVertex(glm::vec3(0.5, 0.5, 0.5));
  _geometry.pushVertex(glm::vec3(0.5, 0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, 0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, 0.5, 0.5));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.pushVertex(glm::vec3(0.5, -0.5, -0.5));
  _geometry.pushVertex(glm::vec3(0.5, -0.5, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, -0.5));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.build();
  scale(glm::vec3(0.5, 0.5, 0.5));
  return (true);
}

void	BonusGraph::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
}

void	BonusGraph::draw(gdl::BasicShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  switch (_type)
  {
    case 0:
      _texturebomb->bind();
    break;
    case 1:
      _texturespeed->bind();
    break;
    case 2:
      _texturefire->bind();
    break;
  }
  if (_i == 20)
  {
    if (_vitesse > 0)
      _vitesse = -0.005;
    else if (_vitesse < 0)
      _vitesse = 0.005;
    _i = 0;
  }
  _i++;
  translate(glm::vec3(0, _vitesse, 0));
  rotate(glm::vec3(0, 1, 0), 1);
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

void  BonusGraph::setActive(Bonus *bonus, int x, int y, int typebonus)
{
  _bonus = bonus;
  _type = typebonus;
  translate(glm::vec3(x, 0, y));
  _active = true;
}

void  BonusGraph::unActive()
{
  _active = false;
  _position = glm::vec3(0, 0, 0);
}

bool  BonusGraph::checkActive()
{
  return (_active);
}

Bonus *BonusGraph::getBonus() const
{
  return (_bonus);
}

glm::vec3 BonusGraph::getPos()
{
  return (_position);
}
