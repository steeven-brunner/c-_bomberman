//
// Error.cpp for Error in /home/gaessl_m/rendu/cpp_plazza/Error
//
// Made by Marin Gaessler
// Login   <gaessl_m@epitech.net>
//
// Started on  Thu Apr 23 13:31:24 2015 Marin Gaessler
// Last update Thu Apr 23 14:00:53 2015 Marin Gaessler
//

#include "Error.hh"

Error::Error(std::string const &message, std::string const &component)
  :	_message(message), _component(component)
{
}

Error::~Error() throw()
{
}

const char*	Error::what() const throw()
{
  return (_message.c_str());
}

const char*	Error::getComponent() const
{
  return (_component.c_str());
}

ThreadError::ThreadError(std::string const &message, std::string const &component)
  :	Error(message, component)
{
}

ThreadError::~ThreadError() throw()
{
}

SystemError::SystemError(std::string const &message, std::string const &component)
  :	Error(message, component)
{
}

SystemError::~SystemError() throw()
{
}

GraphicError::GraphicError(std::string const &message, std::string const &component)
  :	Error(message, component)
{
}

GraphicError::~GraphicError() throw()
{
}

MapError::MapError(std::string const &message, std::string const &component)
  :	Error(message, component)
{
}

MapError::~MapError() throw()
{
}
