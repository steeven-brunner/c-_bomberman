//
// HardObject.cpp for Bomberman in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 16:13:58 2015 Steeven Brunner
// Last update Wed May 13 12:44:17 2015 Steeven Brunner
//

#include "HardObject.hh"

HardObject::HardObject(float posX, float posY)
  : Object(posX, posY, true)
{
}

HardObject::~HardObject()
{
}
