//
// Bomberman.cpp for Bomberman in /home/schler_e/Projet Epitech/C++/cpp_bomberman/Game/
//
// Made by Edwin SCHLERET
// Login   <schler_e@epitech.eu>
//
// Started on  Tue May  5 12:41:17 2015 Edwin SCHLERET
// Last update Sun Jun 14 23:38:29 2015 Edwin SCHLERET
//

#include "Bomberman.hh"
#include "LoadMap.hh"
#include "SaveMap.hh"

Bomberman::Bomberman(std::string namesave, int x, int y)
  : _x(x), _y(y), _end(false), _pause(false), _display(this, false, x, y)
{
  _condVar.init(&_mutex);
  LoadMap map(namesave, this);
  _nbrPlayer = _player.size();
  _nbrPerso = _player.size() + _ia.size();
  if (_nbrPlayer == 2)
    _display.setMulti(true);
}

Bomberman::Bomberman(std::string namemap, std::list<structPlayer> & player, std::list<structIA> & ia, int x, int y)
  : _x(x), _y(y), _nbrPlayer(player.size()), _nbrPerso(player.size() + ia.size()), _end(false), _pause(false), _display(this, _nbrPlayer == 2 ? true : false, x, y)
{
  _condVar.init(&_mutex);
  LoadMap map(namemap, this, player, ia);

  startGame();
}

Bomberman::Bomberman(int sizeX, int sizeY, std::list<structPlayer> & player, std::list<structIA> & ia, int x, int y)
  : _sizeX(sizeX), _sizeY(sizeY), _x(x), _y(y), _nbrPlayer(player.size()), _nbrPerso(player.size() + ia.size()), _end(false), _pause(false), _display(this, _nbrPlayer == 2 ? true : false, x, y)
{

  _condVar.init(&_mutex);
  GenerateMap map(this, player, ia);

  startGame();
}

Bomberman::~Bomberman()
{
  for (std::list<Thread>::iterator it = _thread.begin(); it != _thread.end(); ++it)
    (*it).join();
}

void                Bomberman::startGame()
{
  for (std::list<Player>::iterator it = _player.begin(); it != _player.end(); ++it)
    {
      _thread.emplace_back();
      _thread.back().create(playerLoopFunc, new std::tuple<Bomberman*, int, Player *>(this, (*it).getId(), &(*it)));
    }
  for (std::list<IA>::iterator it = _ia.begin(); it != _ia.end(); ++it)
    {
      _thread.emplace_back();
      _thread.back().create(IALoopFunc, new std::tuple<Bomberman*, int, IA *>(this, (*it).getId(), &(*it)));
    }

  _p1.up = SDLK_z;
  _p1.down = SDLK_s;
  _p1.left = SDLK_q;
  _p1.right = SDLK_d;
  _p1.bomb = SDLK_SPACE;

  _p2.up = SDLK_UP;
  _p2.down = SDLK_DOWN;
  _p2.left = SDLK_LEFT;
  _p2.right = SDLK_RIGHT;
  _p2.bomb = SDLK_KP_0;

  _nameGame = SaveMap::getSaveName();

  _display.initialize();
}

int                 Bomberman::getSizeX() const
{
  return(_sizeX);
}

int                 Bomberman::getSizeY() const
{
  return(_sizeY);
}

void                Bomberman::setSizeX(int x)
{
  _sizeX = x;
}

void                Bomberman::setSizeY(int y)
{
  _sizeY = y;
}

int                 Bomberman::getNbrPlayer() const
{
  return(_nbrPerso);
}

int                 Bomberman::getNbrHuman() const
{
  return(_nbrPlayer);
}

int                 Bomberman::getNbrIA() const
{
  return(_nbrPerso - _nbrPlayer);
}

std::list<Player> * Bomberman::getPlayer()
{
  return(&_player);
}

std::list<IA> *     Bomberman::getIA()
{
  return(&_ia);
}

std::list<Bonus> *  Bomberman::getBonus()
{
  return(&_bonus);
}

std::list<Bomb> *   Bomberman::getBomb()
{
  return(&_bomb);
}

std::list<Wall> *   Bomberman::getWall()
{
  return(&_wall);
}

GameEngine          *Bomberman::getDisplay()
{
  return (&_display);
}

Mutex *              Bomberman::getMutex()
{
    return (&_mutex);
}

void *    Bomberman::IALoopFunc(void *data)
{
  std::get<0>((*static_cast<std::tuple<Bomberman*, int, IA *> *>(data)))->IALoop(std::get<1>((*static_cast<std::tuple<Bomberman*, int, IA *> *>(data))), std::get<2>((*static_cast<std::tuple<Bomberman*, int, IA *> *>(data))));
  return (NULL);
}

void *    Bomberman::playerLoopFunc(void *data)
{
  std::get<0>((*static_cast<std::tuple<Bomberman*, int, Player *> *>(data)))->playerLoop(std::get<1>((*static_cast<std::tuple<Bomberman*, int, Player *> *>(data))), std::get<2>((*static_cast<std::tuple<Bomberman*, int, Player *> *>(data))));
  return (NULL);
}

void      Bomberman::IALoop(int id, IA * ia)
{
  bool    end = false;

  enum move where;
  while(!_end && !end)
  {
    end = true;
    _mutex.lock();
    _condVar.wait();
    _mutex.unlock();
    _mutex.lock();
    for (std::list<IA>::iterator it = _ia.begin(); it != _ia.end() ; ++it)
    {
      if (it->getId() == id)
        end = false;
    }
    if (!end)
    {
      if (ia->getDirection() != NOWHERE)
        ia->move(ia->getDirection());
      ia->setState(SAFTY);
      ia->checkState();
      if (ia->getState() == DANGER)
      {
        if ((where = ia->checkSafe()) != NOWHERE)
          ia->move(where);
        else
          ia->moveSomewhere();
      }
      else if (ia->checkifPlayer() == true)
      {
        ia->placeBomb(*this);
        ia->moveSomewhere();
      }
      else
      {
        if ((where = ia->checkSafe()) != NOWHERE)
          ia->move(where);
      }
    }
    _mutex.unlock();
    usleep(100000);
  }
}

void      Bomberman::checkAction(keybind & key, Player * player)
{
  static move directionEnum[4] = {UP, DOWN, LEFT, RIGHT};
  int direct[4] = {key.up, key.down, key.left, key.right};
  bool    isMove = false;

  for (int i = 0; i < 4; i++)
  {
    if (_input->getKey(direct[i], false) && !isMove)
    {
      isMove = true;
      _display.moveCharacter(player->getId(), directionEnum[i], player->getSpeed());
    }
  }
  if (!isMove)
    _display.moveCharacter(player->getId(), NOWHERE, player->getSpeed());
  if (_input->getKey(key.bomb, false))
    player->placeBomb(*this);
}

void      Bomberman::playerLoop(int id, Player * player)
{
  bool    end = false;

  while (!_end && !end)
    {
      _mutex.lock();
      _condVar.wait();
      _mutex.unlock();
      _mutex.lock();
      end = true;
      for (std::list<Player>::iterator it = _player.begin(); it != _player.end() ; ++it)
      {
        if (it->getId() == id)
          end = false;
      }
      if (!end)
        end = !player->isAlive();
      if (id == 0 && !end)
        checkAction(_p1, player);
      else if (id == 1 && !end)
        checkAction(_p2, player);
      _mutex.unlock();
    }
  if (end)
    std::cout << "Joueur " << id << " est mort" << std::endl;
}

void  Bomberman::CheckEnd()
{
  std::list<Player>                   *player = getPlayer();
  std::list<IA>                       *ia = getIA();

  std::list<Player>::iterator         itPlayer;
  std::list<IA>::iterator             itIA;

  itPlayer = player->begin();
  itIA = ia->begin();
  int  nbChar = 0;

  while (itPlayer != player->end())
    {
      nbChar++;
      ++itPlayer;
    }
  while (itIA != ia->end())
    {
      nbChar++;
      ++itIA;
    }
  if (nbChar == 0 || nbChar == 1)
     _end = true;
}

void      Bomberman::gameLoop()
{
  std::chrono::steady_clock::time_point pause = std::chrono::steady_clock::now();
  bool endechap = false;
  std::string nameWin("");

  while (!_end)
    {
      _display.update();
      _input = _display.getInput();
      if (_input->getKey(SDLK_ESCAPE, false))
      {
        _end = true;
        endechap = true;
      }
      if (_player.size() == 0)
        _end = true;
      if (_input->getKey(SDLK_p, false) && (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - pause).count() > 0.1))
	{
	  std::cout << "pause" << std::endl;
	  _pause = !_pause;
	  pause = std::chrono::steady_clock::now();
	}
      if (_input->getKey(SDLK_o, false))
	{
	  std::cout << "save" << std::endl;
	  SaveMap save(*this, _nameGame);
	}
      if (!_pause)
	{
	  _bomb.front().checkBombList(*this);
	  _condVar.broadcast();
	}
      _display.draw();
      CheckEnd();
    }
  if (endechap != true)
  {
    sleep(1);
    _display.getContext().stop();
    int id;
    if (_player.size() == 0)
      id = -1;
    else
      id = _player.front().getId();
    GameOver *gameover = new GameOver(_x, _y, id);
    if (id != -1)
    {
      if ((nameWin = gameover->inputName()) != "")
        _player.front().setName(nameWin);
    }
    else
      sleep(3);
    delete gameover;
  }
  _condVar.broadcast();
  _score.saveScore(*this);
}
