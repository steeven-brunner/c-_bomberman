//
// Menu.cpp for Menu in /home/gaessl_m/rendu/cpp_bomberman/graphic/menus
//
// Made by Marin GAESSLER
// Login   <gaessl_m@epitech.net>
//
// Started on  Mon May 11 14:30:34 2015 Marin GAESSLER
// Last update Sun Jun 14 18:10:10 2015 Marin Gaessler
//

#include "Menu.hh"

Menu::Menu(int x, int y)
{
  _x = x;
  _y = y;
  _resolution[0] = {.x = 960, .y = 540};
  _resolution[1] = {.x = 1280, .y = 720};
  _resolution[2] = {.x = 1600, .y = 900};
  _resolution[3] = {.x = 1920, .y = 1080};
  _rSize.x = 0;
  _rSize.y = 0;
  _rSize.h = _y;
  _rSize.w = _x;
  _window = NULL;
  _renderer = NULL;
  _texture = NULL;
  _end = false;
  _inputMapX = 10;
  _inputMapY = 10;
  _inputSkillIA = 1;
  _mapCpt = 0;
  _modeCpt = 0;
  _nbPlayers = 1;
  _modes.push_back("1P");
  _modes.push_back("2P");
  _pxl.x = 0;
  _pxl.y = 0;
  _pxl.w = 1;
  _pxl.h = 1;
  _scoreSize = 3;
  initSaves();
  initMaps();
}

Menu::~Menu()
{
  TTF_Quit();
  SDL_Quit();
}

void		Menu::intro()
{
  int w,h;
  bool end = false;

  initWindow();
  _texture = IMG_LoadTexture(_renderer, "textures/intro.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);
  SDL_RenderPresent(_renderer);

  while (!end)
    {
      if (SDL_WaitEvent(&_e))
	{
	  if (_e.type == SDL_QUIT)
	    break;
	  else if (_e.type == SDL_KEYDOWN)
	    {
	      if (_e.key.keysym.sym == SDLK_ESCAPE ||
		  _e.key.keysym.sym == SDLK_RETURN ||
		  _e.key.keysym.sym == SDLK_SPACE)
		end = true;
	    }
	}
    }
}

int		Menu::init()
{
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
      std::cout << "SDL initialization failed." << std::endl;
      return -1;
    }
  if (TTF_Init() == -1)
    {
      std::cout << "SDL_TTF initialization failed." << std::endl;
      return -1;
    }
  if ((_police = TTF_OpenFont("fonts/Super Mario Bros.ttf", 500)) == NULL)
    {
      std::cout << TTF_GetError() << std::endl;
      return -1;
    }
  if (SDL_Init(SDL_INIT_AUDIO) < 0)
    {
      std::cout << "SDL audio initialization failed." << std::endl;
      return 0;
    }
  if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
  {
    std::cout << "cannot load sound" << std::endl;
    return 0;
  }
if ((_music = Mix_LoadMUS("misc/Darude - Sandstorm.mp3")) == NULL)
    {
    std::cout << "cannot load music" << std::endl;
    return 0;
  }
  else
  {
    if( Mix_PlayMusic(_music, -1 ) == -1 )
      {
      std::cout << "error playing music" << std::endl;
      return 0;
    }
    else
      Mix_VolumeMusic(80);
  }
  return 0;
}

void  Menu::fillSave(std::string fileName, int id)
{
  try {
    Bomberman   b(fileName, _x, _y);
    structSave  s;

    s.empty = false;
    s.id = id;
    s.nbPlayers = b.getNbrHuman();
    s.nbIAs = b.getNbrIA();
    s.nbPlayersAlive = b.getPlayer()->size();
    s.nbIAsAlive = b.getIA()->size();
    _saves.push_back(s);
  } catch (Error& ne) {
    std::cout << ne.getComponent() << ": \"" << ne.what() << "\"" << std::endl;
  }
}

void  Menu::initSaves()
{
  int cpt = 1;

  while (cpt <= 6)
  {
    std::string save = "saves/Game" + std::to_string(cpt);
    save += ".save";
    std::ifstream file(save);
    if (file.is_open())
      fillSave(save, cpt);
    else
      _saves.push_back({true, cpt, 0, 0, 0, 0});
    cpt++;
  }
}

void  Menu::fillMaps(std::string fileName)
{
  structMap           m;
  char                str[1024];
  std::string         s;
  std::istringstream  words;

  m.fileName = fileName;

  std::filebuf fbuff;
  fbuff.open (fileName,std::ios::in);

  if (fbuff.is_open())
  {
    std::istream stream(&fbuff);

    if (!stream.eof())
      stream.getline(str, 1024);
    words.clear();
    words.str(str);
    words >> s;
    if (s.find("Name") != std::string::npos)
      words >> s;
    m.mapName = s;
    _maps.push_back(m);
    fbuff.close();
  }
}

void  Menu::initMaps()
{
  int   cpt = 1;

  while (cpt <= 9)
  {
    std::string map = "maps/Map" + std::to_string(cpt);
    map += ".map";
    std::ifstream file(map);
    if (file.is_open())
      fillMaps(map);
    cpt++;
  }
}

void  Menu::startGame()
{
  structPlayer p1 = {0, "P1"};
  _player.push_back(p1);
  if (_nbPlayers == 2)
  {
    p1 = {1, "P2"};
    _player.push_back(p1);
  }
  for (int i = 2; i < _inputNbIA + 2; i++)
    initIA(i + 1);
  try {
    Bomberman   game(_inputMapX, _inputMapY, _player, _ia, _x, _y);
    game.gameLoop();
    } catch (Error& ne) {
      std::cout << ne.getComponent() << ": \"" << ne.what() << "\"" << std::endl;
    }
}

void  Menu::startGameCustomMap()
{
  std::list<structMap>::iterator it;
  size_t                             cpt = 0;

  structPlayer p1 = {0, "P1"};
  _player.push_back(p1);
  if (_nbPlayers == 2)
  {
    p1 = {1, "P2"};
    _player.push_back(p1);
  }
  for (int i = 2; i < _inputNbIA + 2; i++)
    initIA(i + 1);
  for (it = _maps.begin(); cpt < _mapCpt; ++it)
    cpt++;
  try {
    Bomberman   game((*it).fileName, _player, _ia, _x, _y);
    game.gameLoop();
    } catch (Error& ne) {
      std::cout << ne.getComponent() << ": \"" << ne.what() << "\"" << std::endl;
    }
}

void  Menu::initWindow()
{
  _window = SDL_CreateWindow("Bomberman", 0, 0, _x, _y, 0);
  _rSize.h = _y;
  _rSize.w = _x;
  SDL_GetCurrentDisplayMode(0, &_current);
  _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
  if (_current.w == _x && _current.h == _y)
    SDL_SetWindowFullscreen(_window, SDL_WINDOW_FULLSCREEN);
  else if (_current.w < _x || _current.h < _y)
    {
      std::cout << "Error with current resolution, please use a smaller one." << std::endl;
      return;//THROW EXCEPTION
    }
}

void  Menu::destroyWindow()
{
  if (_texture != NULL)
    SDL_DestroyTexture(_texture);
  SDL_DestroyRenderer(_renderer);
  SDL_DestroyWindow(_window);
}

void	Menu::changeResolution(Resolution r)
{
  _x = r.x;
  _y = r.y;
  destroyWindow();
  initWindow();
}

void  Menu::displayTextInMenu(SDL_Color color, const char *str, int x, int y, int w, int h)
{
  SDL_Rect  r;

  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;
  _surfaceMsg = TTF_RenderText_Solid(_police, str, color);
  _texture = SDL_CreateTextureFromSurface(_renderer, _surfaceMsg);
  SDL_RenderCopy(_renderer, _texture, NULL, &r);
  SDL_DestroyTexture(_texture);
  SDL_FreeSurface(_surfaceMsg);
}

void  Menu::initIA(int id)
{
  structIA i = {id, "IA" + std::to_string(id), static_cast<difficulty>(_inputSkillIA)};
  _ia.push_back(i);
}

void  Menu::initMapsAndSaves()
{
  int w, h;

  SDL_RenderClear(_renderer);
  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);

  displayTextInMenu(BLACK, "LOAD MAP", (_x / 10), (_y / 5), (_x / 2), (_y / 10 * 2));
  displayTextInMenu(GREY, "LOAD MAP", (_x / 10 + 5), (_y / 5), (_x / 2), (_y / 10 * 2));
  displayTextInMenu(BLACK, "LOAD SAVE", (_x / 10), (_y / 5 * 3), (_x / 2), (_y / 10 * 2));
  displayTextInMenu(WHITE, "LOAD SAVE", (_x / 10 + 5), (_y / 5 * 3), (_x / 2), (_y / 10 * 2));

  SDL_RenderPresent(_renderer);
}

void  Menu::displayMapsAndSaves(int cursor)
{
  SDL_Rect  rect;

  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  rect.x = (_x / 10);
  rect.y = (_y / 5);
  rect.w = (_x / 2);
  rect.h = (_y / 5 * 3);
  SDL_RenderCopy(_renderer, _texture, &_pxl, &rect);
  SDL_DestroyTexture(_texture);

  displayTextInMenu(BLACK, "LOAD MAP", (_x / 10), (_y / 5), (_x / 2), (_y / 10 * 2));
  displayTextInMenu((cursor == 0 ? GREY : WHITE), "LOAD MAP", (_x / 10 + 5), (_y / 5), (_x / 2), (_y / 10 * 2));
  displayTextInMenu(BLACK, "LOAD SAVE", (_x / 10), (_y / 5 * 3), (_x / 2), (_y / 10 * 2));
  displayTextInMenu((cursor != 0 ? GREY : WHITE), "LOAD SAVE", (_x / 10 + 5), (_y / 5 * 3), (_x / 2), (_y / 10 * 2));
}

void  Menu::initSettingsLoadedMap()
{
  int w, h;

  SDL_RenderClear(_renderer);
  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);

  SDL_RenderPresent(_renderer);

  displayTextInMenu(BLACK, "CUSTOM MAP :", (_x / 7), (_y / 12), (_x / 5), (_y / 12));
  displayTextInMenu(BLACK, "GAME MODE :", (_x / 7), (_y / 12 * 4), (_x / 5), (_y / 12));
  displayTextInMenu(BLACK, "NB IAS :", (_x / 7), (_y / 12 * 7), (_x / 5), (_y / 12));
  displayTextInMenu(BLACK, "IA'S SKILL :", (_x / 7), (_y / 12 * 10), (_x / 5), (_y / 12));
}

void  Menu::displaySettingsLoadedMap(int cursor)
{
  SDL_Rect      brect;
  SDL_Texture*  t;
  std::string   tmp;
  size_t           cpt = 0;
  std::list<std::string>::iterator it2;
  std::list<structMap>::iterator it;

  t = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");

  for (it = _maps.begin(); cpt < _mapCpt; ++it)
    cpt++;
  tmp = (*it).mapName;
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12);
  brect.w = (_x / 10 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12), (_x / 10), (_y / 12));

  cpt = 0;

  for (it2 = _modes.begin(); cpt < _modeCpt; ++it2)
    cpt++;
  tmp = (*it2);
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12 * 4);
  brect.w = (_x / 10 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12 * 4), (_x / 10), (_y / 12));

  tmp = std::to_string(_inputNbIA);
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12 * 7);
  brect.w = (_x / 10 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12 * 7), (_x / 10), (_y / 12));

  tmp = Skills[_inputSkillIA];
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12 * 10);
  brect.w = (_x / 10 * 2 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12 * 10), (_x / 10), (_y / 12));

  displayTextInMenu((cursor != 0 ? WHITE : GREY), "CUSTOM MAP :", (_x / 7 + 5), (_y / 12), (_x / 5), (_y / 12));
  displayTextInMenu((cursor != 1 ? WHITE : GREY), "GAME MODE :", (_x / 7 + 5), (_y / 12 * 4), (_x / 5), (_y / 12));
  displayTextInMenu((cursor != 2 ? WHITE : GREY), "NB IAS :", (_x / 7 + 5), (_y / 12 * 7), (_x / 5), (_y / 12));
  displayTextInMenu((cursor != 3 ? WHITE : GREY), "IA'S SKILL :", (_x / 7 + 5), (_y / 12 * 10), (_x / 5), (_y / 12));

  cpt = 0;
  for (it = _maps.begin(); cpt < _mapCpt; ++it)
    cpt++;
  tmp = (*it).mapName;
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12), (_x / 10), (_y / 12));

  cpt = 0;
  for (it2 = _modes.begin(); cpt < _modeCpt; ++it2)
    cpt++;
  tmp = (*it2);
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12 * 4), (_x / 10), (_y / 12));

  tmp = std::to_string(_inputNbIA);
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12 * 7), (_x / 10), (_y / 12));

  tmp = Skills[_inputSkillIA];
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12 * 10), (_x / 10), (_y / 12));
}

void  Menu::inputLoadedMap(int cursorY, int opt)
{
  switch (cursorY)
  {
    case 0:
      if (opt == 1 && _mapCpt > 0)
        _mapCpt--;
      else if (opt == 2 && _mapCpt < (_maps.size() - 1))
        _mapCpt++;
      break;
    case 1:
      if (opt == 1 && _modeCpt > 0)
      {
        _modeCpt--;
        _nbPlayers = 1;
      }
      else if (opt == 2 && _modeCpt < (_modes.size() - 1))
      {
        _modeCpt++;
        _nbPlayers = 2;
      }
      break;
    case 2:
      if (opt == 1 && _inputNbIA > (_nbPlayers == 1 ? 1 : 0))
        _inputNbIA--;
      else if (opt == 2 && _inputNbIA < MAX_IA)
        _inputNbIA++;
      break;
    case 3:
      if (opt == 1 && _inputSkillIA > 0)
        _inputSkillIA--;
      else if (opt == 2 && _inputSkillIA < 2)
        _inputSkillIA++;
      break;
  }
}

void  Menu::settingsLoadedMap()
{
  int cursor = 0;
  bool end = false;
  _nbPlayers = 1;
  _inputNbIA = _nbPlayers == 1 ? 1 : 0;

  initSettingsLoadedMap();
  displaySettingsLoadedMap(0);
  while (!end)
  {
    if (SDL_WaitEvent(&_e))
     {
       if (_e.type == SDL_QUIT)
         break;
       else if (_e.type == SDL_KEYDOWN)
         {
           _inputNbIA = _nbPlayers == 1 ? 1 : _inputNbIA;
           switch (_e.key.keysym.sym)
            {
              case SDLK_RETURN:
                destroyWindow();
                startGameCustomMap();
                _end = true;
                end = true;
                break;
              case SDLK_ESCAPE:
                _end = false;
                end = true;
                break;
              case SDLK_UP:
                if (cursor != 0)
                  cursor--;
                else if (cursor == 0)
                  cursor = 3;
                displaySettingsLoadedMap(cursor);
                break;
              case SDLK_DOWN:
                if (cursor != 3)
                  cursor++;
                else if (cursor == 3)
                  cursor = 0;
                displaySettingsLoadedMap(cursor);
                break;
              case SDLK_LEFT:
                inputLoadedMap(cursor, 1);
                displaySettingsLoadedMap(cursor);
                break;
              case SDLK_RIGHT:
                inputLoadedMap(cursor, 2);
                displaySettingsLoadedMap(cursor);
                break;
            }
          }
        if (!end)
          SDL_RenderPresent(_renderer);
      }
  }
}

void  Menu::loadMapsAndSaves()
{
  int cursor = 0;
  bool end = false;

  initMapsAndSaves();
  while (!end)
  {
    if (SDL_WaitEvent(&_e))
     {
       if (_e.type == SDL_QUIT)
         break;
       else if (_e.type == SDL_KEYDOWN)
         {
           switch (_e.key.keysym.sym)
            {
              case SDLK_RETURN:
                if (cursor == 0)
                {
                  settingsLoadedMap();
                  end = _end;
                  if (!end)
                  {
                    initMapsAndSaves();
                    displayMapsAndSaves(cursor);
                  }
                }
                else
                {
                  loadGame();
                  end = _end;
                  if (!end)
                  {
                    initMapsAndSaves();
                    displayMapsAndSaves(cursor);
                  }
                }
                break;
              case SDLK_ESCAPE:
                SDL_RenderClear(_renderer);
                _end = false;
                end = true;
                break;
              case SDLK_UP:
                cursor = (cursor == 0 ? 1 : 0);
                displayMapsAndSaves(cursor);
                break;
              case SDLK_DOWN:
                cursor = (cursor == 0 ? 1 : 0);
                displayMapsAndSaves(cursor);
                break;
            }
         }
     }
    if (!end)
      SDL_RenderPresent(_renderer);
  }
}

void  Menu::initRenderSettings()
{
  std::string   tmp;
  int w, h;

  SDL_RenderClear(_renderer);
  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);
  SDL_RenderPresent(_renderer);

  displayTextInMenu(BLACK, "Map X :", (_x / 7), (_y / 12), (_x / 5), (_y / 12));
  displayTextInMenu(BLACK, "Map Y :", (_x / 7), (_y / 12 * 4), (_x / 5), (_y / 12));
  displayTextInMenu(BLACK, "Nb IAs :", (_x / 7), (_y / 12 * 7), (_x / 5), (_y / 12));
  displayTextInMenu(BLACK, "IA's Skill :", (_x / 7), (_y / 12 * 10), (_x / 5), (_y / 12));
}

void    Menu::displaySettings(int cursorY)
{
  SDL_Rect      brect;
  SDL_Texture*  t;
  std::string   tmp;

  t = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");

  tmp = std::to_string(_inputMapX);
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12);
  brect.w = (_x / 10 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12), (_x / 10), (_y / 12));

  tmp = std::to_string(_inputMapY);
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12 * 4);
  brect.w = (_x / 10 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12 * 4), (_x / 10), (_y / 12));

  tmp = std::to_string(_inputNbIA);
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12 * 7);
  brect.w = (_x / 10 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12 * 7), (_x / 10), (_y / 12));

  tmp = Skills[_inputSkillIA];
  brect.x = (_x / 10 * 4);
  brect.y = (_y / 12 * 10);
  brect.w = (_x / 10 * 2 + 5);
  brect.h = (_y / 12);
  SDL_RenderCopy(_renderer, t, &_pxl, &brect);
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 10 * 4), (_y / 12 * 10), (_x / 10), (_y / 12));

  displayTextInMenu((cursorY != 1 ? WHITE : GREY), "Map X :", (_x / 7 + 5), (_y / 12), (_x / 5), (_y / 12));
  displayTextInMenu((cursorY != 2 ? WHITE : GREY), "Map Y :", (_x / 7 + 5), (_y / 12 * 4), (_x / 5), (_y / 12));
  displayTextInMenu((cursorY != 3 ? WHITE : GREY), "Nb IAs :", (_x / 7 + 5), (_y / 12 * 7), (_x / 5), (_y / 12));
  displayTextInMenu((cursorY != 4 ? WHITE : GREY), "IA's Skill :", (_x / 7 + 5), (_y / 12 * 10), (_x / 5), (_y / 12));

  tmp = std::to_string(_inputMapX);
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12), (_x / 10), (_y / 12));

  tmp = std::to_string(_inputMapY);
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12 * 4), (_x / 10), (_y / 12));

  tmp = std::to_string(_inputNbIA);
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12 * 7), (_x / 10), (_y / 12));

  tmp = Skills[_inputSkillIA];
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 10 * 4 + 5), (_y / 12 * 10), (_x / 10), (_y / 12));
}

void  Menu::inputSettings(int cursorY, int opt)
{
  switch (cursorY)
  {
    case 1:
      if (opt == 1 && _inputMapX > 5)
        _inputMapX--;
      else if (opt == 2)
        _inputMapX++;
      break;
    case 2:
      if (opt == 1 && _inputMapY > 5)
        _inputMapY--;
      else if (opt == 2)
        _inputMapY++;
      break;
    case 3:
      if (opt == 1 && _inputNbIA > (_nbPlayers == 1 ? 1 : 0))
        _inputNbIA--;
      else if (opt == 2 && _inputNbIA < MAX_IA)
        _inputNbIA++;
      break;
    case 4:
      if (opt == 1 && _inputSkillIA > 0)
        _inputSkillIA--;
      else if (opt == 2 && _inputSkillIA < 2)
        _inputSkillIA++;
      break;
  }
}

void	Menu::menuSettings(int nbPlayers)
{
  bool end = false;
  _nbPlayers = nbPlayers;
  _inputNbIA = _nbPlayers == 1 ? 1 : 0;
  int cursorY = 1;
  _end = true;

  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");

  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);
  SDL_RenderPresent(_renderer);
  initRenderSettings();
  displaySettings(1);
  SDL_RenderPresent(_renderer);

  while (!end)
    {
      if (SDL_WaitEvent(&_e))
       {
         if (_e.type == SDL_QUIT)
           break;
         else if (_e.type == SDL_KEYDOWN)
           {
             switch (_e.key.keysym.sym)
              {
                case SDLK_RETURN:
                  destroyWindow();
                  startGame();
                  end = true;
                  break;
                case SDLK_ESCAPE:
                  SDL_RenderClear(_renderer);
                  _end = false;
                  end = true;
                  break;
                case SDLK_UP:
                  if (cursorY != 1)
                    cursorY--;
                  displaySettings(cursorY);
                  break;
                case SDLK_DOWN:
                  if (cursorY != 4)
                    cursorY++;
                  displaySettings(cursorY);
                  break;
                case SDLK_LEFT:
                  inputSettings(cursorY, 1);
                  displaySettings(cursorY);
                  break;
                case SDLK_RIGHT:
                  inputSettings(cursorY, 2);
                  displaySettings(cursorY);
                  break;
              }
           }
       }
      if (!end)
       SDL_RenderPresent(_renderer);
    }
}

void  Menu::loadSave(int cursor)
{
  std::string   tmp = "saves/Game" + std::to_string(cursor + 1) + ".save";

  try {
    Bomberman   b(tmp, _x, _y);
    b.startGame();
    b.gameLoop();
  } catch (Error& ne) {
    std::cout << ne.getComponent() << ": \"" << ne.what() << "\"" << std::endl;
  }
  _end = true;
}

void  Menu::initGameLoad()
{
  int       w, h;
  SDL_Rect  r;

  r.x = 0;
  r.y = 0;
  r.w = 1;
  r.h = 1;

  SDL_RenderClear(_renderer);
  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, &r, &_rSize);
  SDL_RenderPresent(_renderer);
}

void  Menu::fillGameLoad(int line, int column)
{
  SDL_Rect      r;

  if (line == 0)
    r.y = _y / 2;
  else
    r.y = _y / 6;

  r.x = _x / 8;
  r.w = (_x / 4) * 3;
  r.h = (_y / 3);
  _surface = SDL_CreateRGBSurface(0, 1, 1, 32, 0, 0, 0, 0);
  SDL_FillRect(_surface, NULL, SDL_MapRGB(_surface->format, 100, 100, 100));
  _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
  SDL_RenderCopy(_renderer, _texture, NULL, &r);
  SDL_DestroyTexture(_texture);

  if (column != 0)
  {
    r.x = _x / 8;
    r.y = _y / 6 + (_y / 3) * line;
    r.w = _x / 8 * 2;
    r.h = _y / 3;
    _surface = SDL_CreateRGBSurface(0, 1, 1, 32, 0, 0, 0, 0);
    SDL_FillRect(_surface, NULL, SDL_MapRGB(_surface->format, 100, 100, 100));
    _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
    SDL_RenderCopy(_renderer, _texture, NULL, &r);
    SDL_DestroyTexture(_texture);
  }

  if (column != 1)
  {
    r.x = _x / 8 * 3;
    r.y = _y / 6 + (_y / 3) * line;
    _surface = SDL_CreateRGBSurface(0, 1, 1, 32, 0, 0, 0, 0);
    SDL_FillRect(_surface, NULL, SDL_MapRGB(_surface->format, 100, 100, 100));
    _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
    SDL_RenderCopy(_renderer, _texture, NULL, &r);
    SDL_DestroyTexture(_texture);
  }

  if (column != 2)
  {
    r.x = _x / 8 * 5;
    r.y = _y / 6 + (_y / 3) * line;
    _surface = SDL_CreateRGBSurface(0, 1, 1, 32, 0, 0, 0, 0);
    SDL_FillRect(_surface, NULL, SDL_MapRGB(_surface->format, 100, 100, 100));
    _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
    SDL_RenderCopy(_renderer, _texture, NULL, &r);
    SDL_DestroyTexture(_texture);
  }

  r.x = (_x / 8) + (_x / 4 * column) + 5;
  r.y = (_y / 6) + (_y / 3 * line) + 5;
  r.w = (_x / 4) - 10;
  r.h = (_y / 3) - 10;

  _surface = SDL_CreateRGBSurface(0, 1, 1, 32, 0, 0, 0, 0);
  SDL_FillRect(_surface, NULL, SDL_MapRGB(_surface->format, 128, 128, 128));
  _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
  SDL_RenderCopy(_renderer, _texture, NULL, &r);
  SDL_DestroyTexture(_texture);
}

void  Menu::displaySaves(struct structSave save, int cpt)
{
  SDL_Rect      r;
  std::string   tmp;

  if (!save.empty)
  {
    r.h = ((_y / 3) - 10) / 8;

    tmp = "Save " + std::to_string(save.id);
    displayTextInMenu(BLACK, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 2), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (0 * 2)) + 5), (_x / 4 - 10), (r.h));

    tmp = std::to_string(save.nbPlayers) + "P // " + std::to_string(save.nbIAs) + (save.nbIAs > 1 ? "IAs" : "IA");
    displayTextInMenu(BLACK, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 2), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (1 * 2)) + 5), (_x / 4 - 10), (r.h));

    tmp = "Nb Players Alive : " + std::to_string(save.nbPlayersAlive);
    displayTextInMenu(BLACK, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 2), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (2 * 2)) + 5), (_x / 4 - 10), (r.h));

    tmp = "Nb IAs Alive : " + std::to_string(save.nbIAsAlive);
    displayTextInMenu(BLACK, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 2), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (3 * 2)) + 5), (_x / 4 - 10), (r.h));

    tmp = "Save " + std::to_string(save.id);
    displayTextInMenu(WHITE, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 7), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (0 * 2)) + 5), (_x / 4 - 10), (r.h));

    tmp = std::to_string(save.nbPlayers) + "P // " + std::to_string(save.nbIAs) + (save.nbIAs > 1 ? "IAs" : "IA");
    displayTextInMenu(WHITE, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 7), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (1 * 2)) + 5), (_x / 4 - 10), (r.h));

    tmp = "Nb Players Alive : " + std::to_string(save.nbPlayersAlive);
    displayTextInMenu(WHITE, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 7), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (2 * 2)) + 5), (_x / 4 - 10), (r.h));

    tmp = "Nb IAs Alive : " + std::to_string(save.nbIAsAlive);
    displayTextInMenu(WHITE, tmp.c_str(), (_x / 8 + ((_x / 4) * (cpt % 3)) + 7), (_y / 6 + ((_y / 3) * (cpt / 3)) + 5 + (r.h * (3 * 2)) + 5), (_x / 4 - 10), (r.h));
  }
  else
  {
    displayTextInMenu(BLACK, "/", (_x / 8 + ((_x / 4) * (cpt % 3) + _x / 16) + 5), (_y / 6 + ((_y / 3) * (cpt / 3) + _y / 12) + 5), (((_x / 4) - 5) / 2), (((_y / 3) - 5) / 2));
    displayTextInMenu(WHITE, "/", (_x / 8 + ((_x / 4) * (cpt % 3) + _x / 16) + 10), (_y / 6 + ((_y / 3) * (cpt / 3) + _y / 12) + 5), (((_x / 4) - 5) / 2), (((_y / 3) - 5) / 2));
  }
}

void  Menu::displayGameLoad(int cursor)
{
  SDL_Rect      r;
  int           cpt = 0;

  r.x = _x / 8;
  r.y = _y / 6;
  r.w = (_x / 4) * 3;
  r.h = (_y / 3) * 2;
  _surface = SDL_CreateRGBSurface(0, (_x / 4) * 3, (_y / 3 * 2), 32, 0, 0, 0, 0);
  SDL_FillRect(_surface, NULL, SDL_MapRGB(_surface->format, 255, 255, 255));
  _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
  SDL_RenderCopy(_renderer, _texture, NULL, &r);
  SDL_DestroyTexture(_texture);

  fillGameLoad((cursor / 3), (cursor % 3));
  for (std::vector<structSave>::iterator it = _saves.begin(); it != _saves.end(); ++it)
  {
    displaySaves(*it, cpt);
    cpt++;
  }
  SDL_RenderPresent(_renderer);
}

bool  Menu::checkSave(int cursor)
{
  std::vector<structSave>::iterator it;
  int   cpt = 0;

  for (it = _saves.begin(); cpt < cursor; ++it)
  {
    cpt++;
  }
  return (!(*it).empty);
}

void  Menu::loadGame()
{
  bool                              end = false;
  int                               cursor = 0;
  std::vector<structSave>::iterator it;
  int                               i = 0;
  std::string                       filename;

  initGameLoad();
  displayGameLoad(cursor);
  SDL_RenderPresent(_renderer);

  while (!end)
  {

      if (SDL_WaitEvent(&_e))
       {
         if (_e.type == SDL_QUIT)
           break;
         else if (_e.type == SDL_KEYDOWN && _e.key.repeat == 0)
           {
             switch (_e.key.keysym.sym)
              {
                case SDLK_RETURN:
                  if (checkSave(cursor))
                  {
                    _end = true;
                    end = true;
                    destroyWindow();
                    loadSave(cursor);
                  }
                  break;
                case SDLK_ESCAPE:
                  SDL_RenderClear(_renderer);
                  _end = false;
                  end = true;
                  break;
                case SDLK_DELETE:
                  i = 0;
                  for (it = _saves.begin(); i < cursor; ++it)
                  {
                    i++;
                  }
                  if (!(*it).empty)
                  {
                    structSave      s;

                    it = _saves.erase(it);
                    s.empty = true;
                    s.id = cursor + 1;
                    s.nbPlayers = 0;
                    s.nbIAs = 0;
                    s.nbPlayersAlive = 0;
                    s.nbIAsAlive = 0;
                    _saves.insert(it, s);
                    filename = "Game" + std::to_string(cursor + 1) + ".save";
                    remove(filename.c_str());
                  }
                  displayGameLoad(cursor);
                  break;
                case SDLK_UP:
                  if (cursor < 3)
                    cursor += 3;
                  else
                    cursor -= 3;
                  displayGameLoad(cursor);
                  break;
                case SDLK_DOWN:
                  if (cursor < 3)
                    cursor += 3;
                  else
                    cursor -= 3;
                  displayGameLoad(cursor);
                  break;
                case SDLK_LEFT:
                  if (cursor != 0 && cursor != 3)
                    cursor--;
                  else
                    cursor += 2;
                  displayGameLoad(cursor);
                  break;
                case SDLK_RIGHT:
                  if (cursor != 2 && cursor != 5)
                    cursor++;
                  else
                    cursor -= 2;
                  displayGameLoad(cursor);
                  break;
              }
           }
       }
  }
}

void  Menu::initRankings()
{
  int       w, h;

  SDL_RenderClear(_renderer);
  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);
  SDL_RenderPresent(_renderer);
}

void  Menu::displayTopRankings(std::list<std::tuple<std::string, int> >  list)
{
  std::list<std::tuple<std::string, int> >::iterator  it = list.begin();
  std::string                                         tmp;
  size_t                                              cpt = 0;

  tmp = "TOP SCORES";
  displayTextInMenu(BLACK, tmp.c_str(), (_x / 12 * 3), (_y / 9), (_x / 12 * 6), (_y / 9));

  tmp = "TOP SCORES";
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 12 * 3 + 5), (_y / 9), (_x / 12 * 6), (_y / 9));

  if (!list.empty())
    {
      if (cpt < _scoreSize)
	{
	  tmp = std::get<0>(*it) + " : " + std::to_string(std::get<1>(*it)) + " points";
	  displayTextInMenu(BLACK, tmp.c_str(), (_x / 12 * 3), (_y / 9 * 3), (_x / 12 * 6), (_y / 9));
	  ++it;
	  cpt++;
	}

      if (cpt < _scoreSize)
	{
	  tmp = std::get<0>(*it) + " : " + std::to_string(std::get<1>(*it)) + " points";
	  displayTextInMenu(BLACK, tmp.c_str(), (_x / 12 * 4), (_y / 9 * 5), (_x / 12 * 4), (_y / 9));
	  ++it;
	  cpt++;
	}

      if (cpt < _scoreSize)
	{
	  tmp = std::get<0>(*it) + " : " + std::to_string(std::get<1>(*it)) + " points";
	  displayTextInMenu(BLACK, tmp.c_str(), (_x / 12 * 4), (_y / 9 * 7), (_x / 12 * 4), (_y / 9));
	  cpt++;
	}

      it = list.begin();
      cpt = 0;

      if (cpt < _scoreSize)
	{
	  tmp = std::get<0>(*it) + " : " + std::to_string(std::get<1>(*it)) + " points";
	  displayTextInMenu(WHITE, tmp.c_str(), (_x / 12 * 3 + 5), (_y / 9 * 3), (_x / 12 * 6), (_y / 9));
	  ++it;
	  cpt++;
	}

      if (cpt < _scoreSize)
	{
	  tmp = std::get<0>(*it) + " : " + std::to_string(std::get<1>(*it)) + " points";
	  displayTextInMenu(WHITE, tmp.c_str(), (_x / 12 * 4 + 5), (_y / 9 * 5), (_x / 12 * 4), (_y / 9));
	  ++it;
	  cpt++;
	}

      if (cpt < _scoreSize)
	{
	  tmp = std::get<0>(*it) + " : " + std::to_string(std::get<1>(*it)) + " points";
	  displayTextInMenu(WHITE, tmp.c_str(), (_x / 12 * 4 + 5), (_y / 9 * 7), (_x / 12 * 4), (_y / 9));
	  cpt++;
	}
    }
}

void	Menu::displayRankings()
{
  Score   s;
  std::list<std::tuple<std::string, int> >  list;
  bool end = false;

  initRankings();
  list = s.loadScore();
  _scoreSize = (list.size() < _scoreSize ? list.size() : _scoreSize);
  list = s.getTop(list, _scoreSize);
  displayTopRankings(list);

  while (!end)
  {
    if (SDL_WaitEvent(&_e))
     {
       if (_e.type == SDL_QUIT)
         break;
       else if (_e.type == SDL_KEYDOWN && _e.key.repeat == 0)
         {
           switch (_e.key.keysym.sym)
            {
              case SDLK_ESCAPE:
                SDL_RenderClear(_renderer);
                return;
                break;
            }
         }
      }
    SDL_RenderPresent(_renderer);
  }
}

void	Menu::displayResolutionOptions(int i)
{
  std::string 	tmp;
  SDL_Rect  rect;

  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  rect.x = (_x / 5);
  rect.y = (_y / 7 * 3);
  rect.w = (_x / 5 * 2 + 5);
  rect.h = (_y / 7);
  SDL_RenderCopy(_renderer, _texture, &_pxl, &rect);
  SDL_DestroyTexture(_texture);

  tmp = std::to_string(_resolution[i].x) + "x" + std::to_string(_resolution[i].y);

  displayTextInMenu(BLACK, tmp.c_str(), (_x / 5), (_y / 7 * 3), (_x / 5 * 2), (_y / 7));
  displayTextInMenu(WHITE, tmp.c_str(), (_x / 5 + 5), (_y / 7 * 3), (_x / 5 * 2), (_y / 7));
}

void	Menu::menuOptions()
{
  bool end = false;
  int 	i;

  switch (_x)
  {
    case 960:
      i = 0;
      break;
    case 1280:
      i = 1;
      break;
    case 1600:
      i = 2;
      break;
    case 1920:
      i = 3;
      break;
  }

  SDL_RenderClear(_renderer);
  initRenderOptions();
  displayResolutionOptions(i);
  SDL_RenderPresent(_renderer);

  while (!end)
    {
      if (SDL_WaitEvent(&_e))
	     {
	       if (_e.type == SDL_QUIT)
	         break;
	       else if (_e.type == SDL_KEYDOWN && _e.key.repeat == 0)
	         {
	           switch (_e.key.keysym.sym)
		          {
                case SDLK_ESCAPE:
                  SDL_RenderClear(_renderer);
                  return;
                  break;
		            case SDLK_RETURN:
		              changeResolution(_resolution[i]);
                  end = true;
		              break;
		            case SDLK_LEFT:
		              if (i > 0)
		                i--;
		              displayResolutionOptions(i);
		              break;
		            case SDLK_RIGHT:
		              if (i < 3)
		                i++;
		              displayResolutionOptions(i);
		              break;
		          }
	         }

	     }
      if (!end)
        SDL_RenderPresent(_renderer);
    }
}

void	Menu::initRenderOptions()
{
  int w, h;

  SDL_RenderClear(_renderer);
  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);
  SDL_RenderPresent(_renderer);

  displayTextInMenu(BLACK, "RESOLUTION", (_x / 5), (_y / 7), (_x / 5 * 2), (_y / 7));
  displayTextInMenu(BLACK, "-", ((_x / 5) - (_x / 10)), (_y / 7 * 3), (_x / 10), (_y / 7));
  displayTextInMenu(BLACK, "+", (_x / 5 * 3), (_y / 7 * 3), (_x / 10), (_y / 7));

  displayTextInMenu(WHITE, "RESOLUTION", (_x / 5 + 5), (_y / 7), (_x / 5 * 2), (_y / 7));
  displayTextInMenu(WHITE, "-", ((_x / 5) - (_x / 10) + 5), (_y / 7 * 3), (_x / 10), (_y / 7));
  displayTextInMenu(WHITE, "+", (_x / 5 * 3 + 5), (_y / 7 * 3), (_x / 10), (_y / 7));
}

void	Menu::swapMenu(int cursorSpot)
{
  switch (cursorSpot)
    {
    case 1:
      menuSettings(1);
      break;
    case 2:
      menuSettings(2);
      break;
    case 3:
      loadMapsAndSaves();
      break;
    case 4:
      displayRankings();
      break;
    case 5:
      menuOptions();
      break;
    }
}

void  Menu::displayMainMenu()
{
  int w, h;

  _texture = IMG_LoadTexture(_renderer, "textures/background_100_100_100.png");
  _cursor = IMG_Load("textures/bomb_menu.bmp");
  _surface = IMG_Load("textures/background_100_100_100.png");
  SDL_QueryTexture(_texture, NULL, NULL, &w, &h);
  SDL_RenderCopy(_renderer, _texture, NULL, &_rSize);

  SDL_Rect  rect;

  displayTextInMenu(BLACK, "1 PLAYER GAME", (_x / 9), (_y / 7), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(BLACK, "2 PLAYERS GAME", (_x / 9), (_y / 7 * 2), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(BLACK, "LOAD GAME", (_x / 9), (_y / 7 * 3), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(BLACK, "RANKINGS", (_x / 9), (_y / 7 * 4), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(BLACK, "OPTIONS", (_x / 9), (_y / 7 * 5), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(BLACK, "QUIT GAME", (_x / 9), (_y / 7 * 6), (_x / 9 * 4), (_y / 6 * 0.5));

  displayTextInMenu(WHITE, "1 PLAYER GAME", (_x / 9 + 5), (_y / 7), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(WHITE, "2 PLAYERS GAME", (_x / 9 + 5), (_y / 7 * 2), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(WHITE, "LOAD GAME", (_x / 9 + 5), (_y / 7 * 3), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(WHITE, "RANKINGS", (_x / 9 + 5), (_y / 7 * 4), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(WHITE, "OPTIONS", (_x / 9 + 5), (_y / 7 * 5), (_x / 9 * 4), (_y / 6 * 0.5));
  displayTextInMenu(WHITE, "QUIT GAME", (_x / 9 + 5), (_y / 7 * 6), (_x / 9 * 4), (_y / 6 * 0.5));

  //BOMB CURSOR
  rect.x = (_x / 100);
  rect.y = (_y / 7) * _cursorSpot;
  rect.w = (_x / 9) - (2 * (_x / 100));
  rect.h = (_y / 6) * 0.5;
  SDL_BlitScaled(_cursor, NULL, _cursor, &rect);
  _texture = SDL_CreateTextureFromSurface(_renderer, _cursor);
  SDL_RenderCopy(_renderer, _texture, NULL, &rect);
  SDL_RenderPresent(_renderer);
  SDL_DestroyTexture(_texture);
}

void	Menu::mainMenu()
{
  SDL_Rect  rect;

  _cursorSpot = 1;
  SDL_RenderClear(_renderer);

  displayMainMenu();

  bool end = false;
  while (!end)
    {
      if (SDL_WaitEvent(&_e))
	     {
	       if (_e.type == SDL_QUIT)
	         break;
	       else if (_e.type == SDL_KEYDOWN && _e.key.repeat == 0)
	         {
	           switch (_e.key.keysym.sym)
	     	      {
	     	      case SDLK_RETURN:
	     	        if (_cursorSpot == 6)
	     	          end = true;
	     	        else
	     	          {
	     	            SDL_RenderClear(_renderer);
	     	            swapMenu(_cursorSpot);
                    end = _end;
                    if (!end)
                      displayMainMenu();
	     	          }
	     	        break;
	     	      case SDLK_KP_ENTER:
	     	        if (_cursorSpot == 6)
	     	          end = true;
	     	        else
	     	          {
	     	            SDL_RenderClear(_renderer);
	     	            swapMenu(_cursorSpot);
                    end = _end;
                    if (!end)
                      displayMainMenu();
	     	          }
	     	        break;
	     	      case SDLK_UP:
	     	        if (_cursorSpot != 1)
	     	          {
	     	            _cursorSpot--;
	     	            rect.x = 0;
	     	            rect.y = 0;
	     	            rect.w = (_x / 9);
	     	            rect.h = _y;
	     	            _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
	     	            SDL_RenderCopy(_renderer, _texture, &rect, &rect);
                    SDL_DestroyTexture(_texture);
	     	            rect.x = (_x / 100);
	     	            rect.y = (_y / 7) * _cursorSpot;
	     	            rect.w = (_x / 9) - (2 * (_x / 100));
	     	            rect.h = (_y / 6) * 0.5;
	     	            _texture = SDL_CreateTextureFromSurface(_renderer, _cursor);
	     	            SDL_RenderCopy(_renderer, _texture, NULL, &rect);
                    SDL_DestroyTexture(_texture);
	     	          }
	     	        break;
	     	      case SDLK_DOWN:
	     	        if (_cursorSpot != 6)
	     	          {
	     	            _cursorSpot++;
	     	            rect.x = 0;
	     	            rect.y = 0;
	     	            rect.w = (_x / 9);
	     	            rect.h = _y;
	     	            _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
	     	            SDL_RenderCopy(_renderer, _texture, &rect, &rect);
                    SDL_DestroyTexture(_texture);
	     	            rect.x = (_x / 100);
	     	            rect.y = (_y / 7) * _cursorSpot;
	     	            rect.w = (_x / 9) - (2 * (_x / 100));
	     	            rect.h = (_y / 6) * 0.5;
	     	            _texture = SDL_CreateTextureFromSurface(_renderer, _cursor);
	     	            SDL_RenderCopy(_renderer, _texture, NULL, &rect);
                    SDL_DestroyTexture(_texture);
	     	          }
	     	        break;
	     	      }
	         }
	     }
      if (!end)
        SDL_RenderPresent(_renderer);
    }
}
