//
// Bonus.cpp for Bomberman in /home/brunne_s/rendu/cpp_bomberman/Game
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Tue May  5 17:58:54 2015 Steeven Brunner
// Last update Sun Jun 14 16:08:08 2015 Steeven Brunner
//

#include "Bonus.hh"

Bonus::Bonus(float posX, float posY, BonusType type)
  : SoftObject(posX, posY), _type(type), _isShow(true)
{
}

Bonus::~Bonus()
{
}

BonusType	Bonus::getType() const
{
  return (_type);
}

bool      Bonus::isShow() const
{
  return (_isShow);
}

void      Bonus::setIsShow(bool show)
{
  _isShow = show;
}
