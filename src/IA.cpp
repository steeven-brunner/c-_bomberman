//
// IA.cpp for Bomberman in /home/brunne_s/rendu/cpp_bomberman/src
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Sun Jun 14 16:15:52 2015 Steeven Brunner
// Last update Sun Jun 14 23:14:40 2015 Steeven Brunner
//

#include <tuple>
#include "IA.hh"
#include "Bomberman.hh"

IA::IA(int posX, int posY, std::string name, enum difficulty diff, int id, Bomberman *boss)
  : Player(posX, posY, name, id)
{
  _diff = diff;
  _state = SAFTY;
  _bomberman = boss;
  _i = 0;
  _direction = NOWHERE;
}

IA::IA(int posX, int posY, std::string name, enum difficulty diff, int id, Bomberman *boss, int range, int capacity, int score)
  : Player(posX, posY, name, id, range, capacity, score)
{
  _diff = diff;
  _state = SAFTY;
  _bomberman = boss;
  _i = 0;
  _direction = NOWHERE;
}

IA::~IA()
{
}

enum difficulty		IA::getDifficulty() const
{
  return (_diff);
}

void				IA::setState(enum state newstate)
{
  _state = newstate;
}

enum state 			IA::getState() const
{
  return (_state);
}

enum move				IA::getDirection() const
{
	return (_direction);
}

void				IA::move(enum move where)
{
	switch (where)
	{
		case UP:
			if (_direction == NOWHERE)
			{
				_newPosY = static_cast<int>(_posY) + 1.2;
				_i = 0;
			}
			_bomberman->getDisplay()->moveCharacter(_id, UP);
			_i++;
			if (_posY >= _newPosY || _i > 30)
			{
				_bomberman->getDisplay()->moveCharacter(_id, NOWHERE);
				_direction = NOWHERE;
			}
			break;
		case LEFT:
			if (_direction == NOWHERE)
			{
				_newPosX = static_cast<int>(_posX) + 1.2;
				_i = 0;
			}
			_bomberman->getDisplay()->moveCharacter(_id, LEFT);
			_i++;
			if (_posX >= _newPosX || _i > 30)
			{
				_bomberman->getDisplay()->moveCharacter(_id, NOWHERE);
				_direction = NOWHERE;
			}
			break;
		case RIGHT:
			if (_direction == NOWHERE)
			{
				_newPosX = static_cast<int>(_posX) - 1.2;
				_i = 0;
			}
				_bomberman->getDisplay()->moveCharacter(_id, RIGHT);
			_i++;
			if (_posY <= _newPosY || _i > 30)
			{
				_bomberman->getDisplay()->moveCharacter(_id, NOWHERE);
				_direction = NOWHERE;
			}
			break;
		case DOWN:
			if (_direction == NOWHERE)
			{
				_newPosY = static_cast<int>(_posY) - 1.2;
				_i = 0;
			}
				_bomberman->getDisplay()->moveCharacter(_id, DOWN);
			_i++;
			if (_posY <= _newPosY || _i > 30)
			{
				_bomberman->getDisplay()->moveCharacter(_id, NOWHERE);
				_direction = NOWHERE;
			}
			break;
		default:
			break;
	}
}

void				IA::checkState()
{
  std::tuple<IA *, enum move> up (this, UP);
  std::tuple<IA *, enum move> down (this, DOWN);
  std::tuple<IA *, enum move> left (this, LEFT);
  std::tuple<IA *, enum move> right (this, RIGHT);

  for (int j = 0; j < 4; j++)
    this->_around[j] = TYPE_WALL;
  getNearestObj(&up);
  getNearestObj(&down);
  getNearestObj(&left);
  getNearestObj(&right);
  for (int i = 0; i < 4; i++)
    {
      if (this->_around[i] == TYPE_BOMB)
	this->_state = DANGER;
    }
}

void 				*IA::getNearestObj(void *obj)
{
  std::tuple<IA *, enum move> *realobj;
  int 			x1;
  int 			x2;
  int 			y1;
  int				y2;

  realobj = static_cast<std::tuple<IA *, enum move> *>(obj);
  switch (std::get<1>(*realobj))
    {
    case UP:
      y1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posY + 1);
      y2 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_bomberman->getSizeY());
      x1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posX);
      x2 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posX);
      break;
    case DOWN:
      y1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posY - 1);
      y2 = 0;
      x1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posX);
      x2 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posX);
      break;
    case LEFT:
      y1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posY);
      y2 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posY);
      x1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posX + 1);
      x2 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_bomberman->getSizeX());
      break;
    case RIGHT:
      y1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posY);
      y2 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posY);
      x1 = std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_posX - 1);
      x2 = 0;
      break;
    default:
      break;
    }
  if (x1 < 0 || x1 > std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_bomberman->getSizeX()) || y1 < 0 || y1 > std::get<0>(*realobj)->myToint(std::get<0>(*realobj)->_bomberman->getSizeY()))
    {
      std::get<0>(*realobj)->setAround(std::get<1>(*realobj), TYPE_WALL);
      return (NULL);
    }
  while (x1 != x2)
    {
      if (x1 < x2)
	{
	  x1++;
	  std::get<0>(*realobj)->setAround(std::get<1>(*realobj), std::get<0>(*realobj)->checkPosition(x1, y1));
	  if (std::get<0>(*realobj)->checkPosition(x1, y1) != NOTHING)
	    return (NULL);
	}
      else
	{
	  x2++;
	  std::get<0>(*realobj)->setAround(std::get<1>(*realobj), std::get<0>(*realobj)->checkPosition(x2, y1));
	  if (std::get<0>(*realobj)->checkPosition(x2, y1) != NOTHING)
	    return (NULL);
	}
    }
  while (y1 != y2)
    {
      if (y1 < y2)
	{
	  y1++;
	  std::get<0>(*realobj)->setAround(std::get<1>(*realobj), std::get<0>(*realobj)->checkPosition(x1, y1));
	  if (std::get<0>(*realobj)->checkPosition(x1, y1) != NOTHING)
	    return (NULL);
	}
      else
	{
	  y2++;
	  std::get<0>(*realobj)->setAround(std::get<1>(*realobj), std::get<0>(*realobj)->checkPosition(x1, y2));
	  if (std::get<0>(*realobj)->checkPosition(x1, y2) != NOTHING)
	    return (NULL);
	}
    }
  return(NULL);
}

void 			IA::setAround(int n, enum obj_type obj)
{
	this->_around[n] = obj;
}

enum obj_type	IA::checkPosition(int x, int y)
{
  enum obj_type res = NOTHING;

  for (std::list<Bomb>::iterator it = this->_bomberman->getBomb()->begin(); it != this->_bomberman->getBomb()->end(); it++)
    {
      if (it != _bomberman->getBomb()->end() && this->myToint(it->getPosX()) == x && this->myToint(it->getPosY()) == y)
	{
	  res = TYPE_BOMB;
	}
    }
  for (std::list<Player>::iterator it = this->_bomberman->getPlayer()->begin(); it != this->_bomberman->getPlayer()->end(); it++)
    {
      if (it != _bomberman->getPlayer()->end() && this->myToint(it->getPosX()) == x && this->myToint(it->getPosY()) == y)
	{
	  res = TYPE_PLAYER;
	}
    }
  for (std::list<Wall>::iterator it = this->_bomberman->getWall()->begin(); it != this->_bomberman->getWall()->end(); it++)
    {
      if (it != _bomberman->getWall()->end() && this->myToint(it->getPosX()) == x && this->myToint(it->getPosY()) == y)
	res = TYPE_WALL;
    }
  return (res);
}


bool					IA::checkifPlayer()
{
  std::tuple<IA *, enum move> up (this, UP);
  std::tuple<IA *, enum move> down (this, DOWN);
  std::tuple<IA *, enum move> left (this, LEFT);
  std::tuple<IA *, enum move> right (this, RIGHT);

  getNearestObj(&up);
  getNearestObj(&down);
  getNearestObj(&left);
  getNearestObj(&right);
  for (int i = 0; i < 4; i++)
    {
      if (_around[i] == TYPE_PLAYER)
	return true;
    }
  return false;
}

void					IA::moveSomewhere()
{
  if (this->myToint(this->_posY) + 1 <= this->_bomberman->getSizeY() && checkPosition(this->myToint(this->_posX), this->myToint(this->_posY) + 1) == NOTHING)
    {
      this->move(UP);
      return ;
    }
  if (this->myToint(this->_posX) + 1 <= this->_bomberman->getSizeX() && checkPosition(this->myToint(this->_posX) + 1, this->myToint(this->_posY)) == NOTHING)
    {
      this->move(LEFT);
      return ;
    }
  if (this->myToint(this->_posX) - 1 >= 0 && checkPosition(this->myToint(this->_posX) - 1, this->myToint(this->_posY)) == NOTHING)
    {
      this->move(RIGHT);
      return ;
    }
  if (this->myToint(this->_posY) - 1 >= 0 && checkPosition(this->myToint(this->_posX), this->myToint(this->_posY) - 1) == NOTHING)
    {
      this->move(DOWN);
      return ;
    }
}

enum move				IA::checkSafe()
{
  std::tuple<IA *, enum move> up (this, UP);
  std::tuple<IA *, enum move> down (this, DOWN);
  std::tuple<IA *, enum move> left (this, LEFT);
  std::tuple<IA *, enum move> right (this, RIGHT);

  if (this->myToint(this->_posY) - 1 >= 0 && checkPosition(this->myToint(this->_posX), this->myToint(this->_posY) - 1) == NOTHING)
    {
      this->_posY -= 1;
      getNearestObj(&up);
      getNearestObj(&down);
      getNearestObj(&left);
      getNearestObj(&right);
      this->_posY += 1;
      if (this->_around[0] != TYPE_BOMB && this->_around[1] != TYPE_BOMB && this->_around[2] != TYPE_BOMB && this->_around[3] != TYPE_BOMB)
	return DOWN;
    }
  if (this->myToint(this->_posY)  + 1<= this->_bomberman->getSizeY() && checkPosition(this->myToint(this->_posX), this->myToint(this->_posY) + 1) == NOTHING)
    {
      this->_posY += 1;
      getNearestObj(&up);
      getNearestObj(&down);
      getNearestObj(&left);
      getNearestObj(&right);
      this->_posY -= 1;
      if (this->_around[0] != TYPE_BOMB && this->_around[1] != TYPE_BOMB && this->_around[2] != TYPE_BOMB && this->_around[3] != TYPE_BOMB)
	return UP;
    }
  if (this->myToint(this->_posX) + 1 <= this->_bomberman->getSizeX() && checkPosition(this->myToint(this->_posX) + 1, this->myToint(this->_posY)) == NOTHING)
    {
      this->_posX += 1;
      getNearestObj(&up);
      getNearestObj(&down);
      getNearestObj(&left);
      getNearestObj(&right);
      this->_posX -= 1;
      if (this->_around[0] != TYPE_BOMB && this->_around[1] != TYPE_BOMB && this->_around[2] != TYPE_BOMB && this->_around[3] != TYPE_BOMB)
	return LEFT;
    }
  if (this->myToint(this->_posX) - 1 >= 0 && checkPosition(this->myToint(this->_posX) - 1, this->myToint(this->_posY)) == NOTHING)
    {
      this->_posX -= 1;
      getNearestObj(&up);
      getNearestObj(&down);
      getNearestObj(&left);
      getNearestObj(&right);
      this->_posX += 1;
      if (this->_around[0] != TYPE_BOMB && this->_around[1] != TYPE_BOMB && this->_around[2] != TYPE_BOMB && this->_around[3] != TYPE_BOMB)
	return RIGHT;
    }
  return NOWHERE;
}

int					IA::myToint(float num)
{
  return (static_cast<int>(num));
}
