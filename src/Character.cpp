//
// Character.cpp for Character in /home/dayrie_l/rendu/cpp_bomberman/graphic
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon May 11 13:59:31 2015 Lucas Dayries
// Last update Sun Jun 14 16:10:35 2015 Steeven Brunner
//

#include "Character.hpp"
#include "Bomberman.hh"

Character::Character(Player *player)
  : _player(player)
{
}

Character::~Character()
{
}

bool	Character::initialize()
{
  _speed = 5.0f;
  _state = false;
  if ((_model.load("lib_graphic/assets/marvin.fbx")) == false)
    {
      std::cerr << "Could not load 3D model." << std::endl;
      return (false);
    }
  if ((_model.createSubAnim(0, "subanim", 36, 50)) == false)
    {
      std::cerr << "subAnim is not loaded" << std::endl;
    }
  if ((_model.createSubAnim(0, "subanim_arret", 10, 0)) == false)
    {
      std::cerr << "subAnim is not loaded" << std::endl;
    }
  _player->setPosX(_player->getPosX() + 0.5);
  _player->setPosY(_player->getPosY() + 0.5);
  translate(glm::vec3(_player->getPosX(), 0, _player->getPosY()));
  return (true);
}

void	Character::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
}

void	Character::draw(gdl::BasicShader &shader, gdl::Clock const &clock)
{
  if (_player->isAlive())
  _model.draw(shader, getTransformation(), clock.getElapsed());
}

void    Character::moveCharacter(Bomberman * bom, gdl::Clock const &clock, move dir, float speed)
{
  glm::vec3   pos;

  switch (dir)
  {
    case UP:
      rotate(glm::vec3(0, 1, 0), 0);
      pos = glm::vec3(0, 0, 1) * static_cast<float>(clock.getElapsed()) * (_speed + speed);
      if (_player->movePlayer(*bom, pos.x, pos.z))
        translate(glm::vec3(0, 0, 1) * static_cast<float>(clock.getElapsed()) * (_speed + speed));
      _button = true;
      break;
    case DOWN:
      rotate(glm::vec3(0, 1, 0), 180);
      pos = glm::vec3(0, 0, -1) * static_cast<float>(clock.getElapsed()) * (_speed + speed);
      if (_player->movePlayer(*bom, pos.x, pos.z))
        translate(glm::vec3(0, 0, -1) * static_cast<float>(clock.getElapsed()) * (_speed + speed));
      _button = true;
      break;
    case LEFT:
      rotate(glm::vec3(0, 1, 0), 90);
      pos = glm::vec3(1, 0, 0) * static_cast<float>(clock.getElapsed()) * (_speed + speed);
      if (_player->movePlayer(*bom, pos.x, pos.z))
        translate(glm::vec3(1, 0, 0) * static_cast<float>(clock.getElapsed()) * (_speed + speed));
      _button = true;
      break;
    case RIGHT:
      rotate(glm::vec3(0, 1, 0), 270);
      pos = glm::vec3(-1, 0, 0) * static_cast<float>(clock.getElapsed()) * (_speed + speed);
      if (_player->movePlayer(*bom, pos.x, pos.z))
        translate(glm::vec3(-1, 0, 0) * static_cast<float>(clock.getElapsed()) * (_speed + speed));
      _button = true;
      break;
    default:
      _button = false;
      break;
  }
  if (_button == false && _state == true)
    {
      if ((_model.setCurrentSubAnim("subanim_arret", false)) == false)
	std::cerr << "subanimation is not loaded" << std::endl;
      _state = false;
    }
  if (_button == true && _state == false)
    {
      if ((_model.setCurrentSubAnim("subanim", true)) == false)
	std::cerr << "subanimation is not loaded" << std::endl;
      _state = true;
    }
}

glm::vec4 Character::getColor() const
{
  return (_color);
}

void Character::setColor(glm::vec4 color)
{
  _color = color;
}

Player *      Character::getPlayer()
{
  return (_player);
}
