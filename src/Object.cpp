//
// Object.cpp for Bomberman in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 16:13:58 2015 Steeven Brunner
// Last update Mon Jun  8 15:36:25 2015 Steeven Brunner
//

#include "Object.hh"

Object::Object(float posX, float posY, bool isSolid)
  : _posX(posX), _posY(posY), _isSolid(isSolid)
{
}

Object::~Object()
{
}

float	Object::getPosX() const
{
  return(_posX);
}

float	Object::getPosY() const
{
  return(_posY);
}

void	Object::setPosX(float posX)
{
  _posX = posX;
}

void	Object::setPosY(float posY)
{
  _posY = posY;
}

bool	Object::getType() const
{
  return (_isSolid);
}

bool	Object::getIsSolid() const
{
  return (_isSolid);
}

bool	Object::checkCollision(float x, float y, Object & obj) const
{
  if (x >= static_cast<int>(obj.getPosX()) - 0.2 && x <= static_cast<int>(obj.getPosX()) + 1.2 && y >= static_cast<int>(obj.getPosY()) - 0.2 && y <= static_cast<int>(obj.getPosY()) + 1.2)
    return (true);
  return (false);
}
