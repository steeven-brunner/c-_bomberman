//
// SaveMap.cpp for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/src/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Thu May 21 14:25:54 2015 Victor Ganter
// Last update Sun Jun 14 16:59:38 2015 Victor Ganter
//

#include "SaveMap.hh"

SaveMap::SaveMap(Bomberman &bomberman, std::string str)
{
  std::ostringstream namesave;

  namesave << str << ".save";
  std::ofstream savefile(namesave.str(), std::ofstream::out);
  if(savefile.is_open())
  {
    savefile << "Size X " << bomberman.getSizeX() << std::endl;
    savefile << "Size Y " << bomberman.getSizeY() << std::endl;
    writeWalls(bomberman.getWall(), savefile);
    writePlayers(bomberman.getPlayer(), bomberman.getIA() ,savefile);
    writeBomb(bomberman.getBomb(), savefile);
    writeBonus(bomberman.getBonus(), savefile);
    savefile.close();
  }
}

SaveMap::~SaveMap ()
{
}

int SaveMap::writeWalls(std::list<Wall> *walllist, std::ofstream &savefile)
{
  std::list<Wall>::iterator it;

  for (it = walllist->begin(); it != walllist->end(); it++)
  {
    if (it->getIsBreakable())
      savefile << "WallBreakable ; X " << it->getPosX() << " ; Y " << it->getPosY() << std::endl;
    else
      savefile << "WallUnBreakable ; X " << it->getPosX() << " ; Y " << it->getPosY() << std::endl;
  }
  return (0);
}

int SaveMap::writePlayers(std::list<Player> *playerlist, std::list<IA> *IAlist, std::ofstream &savefile)
{
  for (std::list<Player>::iterator it = playerlist->begin(); it != playerlist->end(); it++)
  {
    savefile << "Player ; X " << static_cast<int>(it->getPosX()) << " ; Y " << static_cast<int>(it->getPosY()) << " ; name " << it->getName() << " ; id " << it->getId() << " ; range " << it->getRange() << " ; capacity " << it->getCapacity() << " ; score " << it->getScore() << std::endl;
  }
  for (std::list<IA>::iterator it = IAlist->begin(); it != IAlist->end(); it++)
  {
    savefile << "IA ; X " << static_cast<int>(it->getPosX()) << " ; Y " << static_cast<int>(it->getPosY()) << " ; name " << it->getName() << " ; diff " << it->getDifficulty() << " ; id " << it->getId() << " ; range " << it->getRange() << " ; capacity " << it->getCapacity() << " ; score " << it->getScore() << std::endl;
  }
  return (0);
}

int SaveMap::writeBomb(std::list<Bomb> *bomblist, std::ofstream &savefile)
{
  std::list<Bomb>::iterator it;

  for (it = bomblist->begin(); it != bomblist->end(); it++)
  {
    savefile << "Bomb ; X " << it->getPosX() << " ; Y " << it->getPosY() << " ; range " << it->getRange() << " ; playerid " << it->getPlayerId() << " ; time " << it->getTime() << std::endl;
  }
  return (0);
}

int SaveMap::writeBonus(std::list<Bonus> *bonuslist, std::ofstream &savefile)
{
  std::list<Bonus>::iterator it;

  for (it = bonuslist->begin(); it != bonuslist->end(); it++)
  {
    savefile << "Bomb ; X " << it->getPosX() << " ; Y " << it->getPosY() << " ; type " << it->getType() << std::endl;
  }
  return (0);
}

std::string       SaveMap::getSaveName()
{
  int             i = 1;
  std::string     name;

  while (i <= 6)
    {
      std::string save = "saves/Game" + std::to_string(i);
      name = save;
      save += ".save";
      std::ifstream file(save);
      if (file.is_open())
        i++;
      else
        return (name);
    }
  return (name);
}
