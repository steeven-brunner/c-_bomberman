//
// LoadMap.cpp for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/Game/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Fri May 15 10:42:47 2015 Victor Ganter
// Last update Sun Jun 14 18:50:06 2015 Victor Ganter
//

#include "../inc/LoadMap.hh"

LoadMap::LoadMap(std::string save, Bomberman *bomberman) : GenerateMap(bomberman)
{
  std::filebuf fbuff;

  if (save.find(".save") == std::string::npos)
    std::cout << "File is not a valid save type" << std::endl;
  else
  {
    fbuff.open (save,std::ios::in);
    if (fbuff.is_open())
    {
      std::istream stream(&fbuff);
      read_save(stream, bomberman);
      fbuff.close();
    }
  }
}

LoadMap::LoadMap(std::string map, Bomberman *bomberman, std::list<structPlayer> player, std::list<structIA> IA) : GenerateMap(bomberman)
{
  std::filebuf fbuff;

  if (map.find(".map") == std::string::npos)
    std::cout << "File is not a valid map type" << std::endl;
  else
  {
    fbuff.open (map,std::ios::in);
    if (fbuff.is_open())
    {
      std::istream stream(&fbuff);
      read_map(stream, bomberman, player, IA);
      fbuff.close();
    }
  }
}

LoadMap::~LoadMap()
{
}

int LoadMap::ParseBreakable(std::istringstream &words, int line)
{
  std::string string;
  std::string params[2] = {"X", "Y"};
  int         i = 0;
  int         x;
  int         y;
  int         j = 0;

  while (j < 2)
  {
    words >> string;
    words >> string;
    i = 0;
    while (i < 2 && string.find(params[i]) == std::string::npos)
      i++;
    if (i >= 2)
    {
      std::cout << "Error when parsing : '" << words.str() << "' at line  " << line << std::endl;
      return (-1);
    }
    if (i == 0)
      words >> x;
    else
      words >> y;
    j++;
  }
  add_cassable(x, y);
  return (0);
}

int LoadMap::ParseUnBreakable(std::istringstream &words, int line)
{
  std::string string;
  std::string params[2] = {"X", "Y"};
  int         i = 0;
  int         x;
  int         y;
  int         j = 0;

  while (j < 2)
  {
    words >> string;
    words >> string;
    i = 0;
    while (i < 2 && string.find(params[i]) == std::string::npos)
      i++;
    if (i >= 2)
    {
      std::cout << "Error when parsing, at line " << line << " in : '" << words.str() << "', '" << string << "' is not a valid parameter" << std::endl;
      return (-1);
    }
    if (i == 0)
      words >> x;
    else
      words >> y;
    j++;
  }
  add_incassable(x, y);
  return (0);
}

int LoadMap::ParseBomb(std::istringstream &words, int line)
{
  std::string string;
  std::string params[5] = {"X", "Y", "range", "playerid", "time"};
  int         i = 0;
  int         x;
  int         y;
  int         range;
  int         playerId;
  int         time;
  int         j = 0;

  while (j < 5)
  {
    words >> string;
    words >> string;
    i = 0;
    while (i < 5 && string.find(params[i]) == std::string::npos)
      i++;
    if (i >= 5)
    {
      std::cout << "Error when parsing, at line " << line << " in : '" << words.str() << "', '" << string << "' is not a valid parameter" << std::endl;
      return (-1);
    }
    switch (i)
    {
      case 0:
        words >> x;
      break;
      case 1:
        words >> y;
      break;
      case 2:
        words >> range;
      break;
      case 3:
        words >> playerId;
      break;
      case 4:
        words >> time;
      break;
    }
    j++;
  }
  add_Bomb(x, y, range, playerId, time);
  return (0);
}

int LoadMap::ParseBonus(std::istringstream &words, int line)
{
  std::string string;
  std::string params[3] = {"X", "Y", "type"};
  int         i = 0;
  int         x;
  int         y;
  int         type;
  int         j = 0;

  while (j < 3)
  {
    words >> string;
    words >> string;
    i = 0;
    while (i < 3 && string.find(params[i]) == std::string::npos)
      i++;
    if (i == 3)
    {
      std::cout << "Error when parsing, at line " << line << " in : '" << words.str() << "', '" << string << "' is not a valid parameter" << std::endl;
      return (-1);
    }
    switch (i)
    {
      case 0:
        words >> x;
      break;
      case 1:
        words >> y;
      break;
      case 2:
        words >> type;
      break;
    }
    j++;
  }
  add_Bonus(x, y, static_cast<BonusType>(type));
  return (0);
}

int LoadMap::ParsePlayer(std::istringstream &words, int line)
{
  std::string string;
  std::string params[7] = {"X", "Y", "name", "id", "range", "capacity", "score"};
  int         i = 0;
  int         x;
  int         y;
  std::string name;
  int         id;
  int         range;
  int         capacity;
  int         score;
  int         j = 0;

  while (j < 7)
  {
    words >> string;
    words >> string;
    i = 0;
    while (i < 7 && string.find(params[i]) == std::string::npos)
      i++;
    if (i == 7)
    {
      std::cout << "Error when parsing, at line " << line << " in : '" << words.str() << "', '" << string << "' is not a valid parameter" << std::endl;
      return (-1);
    }
    switch (i)
    {
      case 0:
        words >> x;
      break;
      case 1:
        words >> y;
      break;
      case 2:
        words >> name;
      break;
      case 3:
        words >> id;
      break;
      case 4:
        words >> range;
      break;
      case 5:
        words >> capacity;
      break;
      case 6:
        words >> score;
      break;
    }
    j++;
  }
  add_player(x, y, name, id, range, capacity, score);
  return (0);
}

int LoadMap::ParseIa(std::istringstream &words, int line)
{
  std::string string;
  std::string params[8] = {"X", "Y", "name", "diff", "id", "range", "capacity", "score"};
  int         i = 0;
  int         x;
  int         y;
  std::string name;
  int         diff;
  int         id;
  int         range;
  int         capacity;
  int         score;
  int         j = 0;

  while (j < 8)
  {
    words >> string;
    words >> string;
    i = 0;
    while (i < 8 && string.find(params[i]) == std::string::npos)
      i++;
    if (i == 8)
    {
      std::cout << "Error when parsing, at line " << line << " in : '" << words.str() << "', '" << string << "' is not a valid parameter" << std::endl;
      return (-1);
    }
    switch (i)
    {
      case 0:
        words >> x;
      break;
      case 1:
        words >> y;
      break;
      case 2:
        words >> name;
      break;
      case 3:
        words >> diff;
      break;
      case 4:
        words >> id;
      break;
      case 5:
        words >> range;
      break;
      case 6:
        words >> capacity;
      break;
      case 7:
        words >> score;
      break;
    }
    j++;
  }
  add_IA(x, y, name, static_cast<enum difficulty>(diff), id, range, capacity, score);
  return (0);
}

int LoadMap::lexCmd(std::string cmd, std::istringstream &words, int line)
{
  int i = 0;
  std::string type[6] = {"WallBreakable", "WallUnBreakable", "Bomb", "Bonus", "Player", "IA"};
  int	(LoadMap::*parsers[6])(std::istringstream &, int line);

  parsers[0] = &LoadMap::ParseBreakable;
  parsers[1] = &LoadMap::ParseUnBreakable;
  parsers[2] = &LoadMap::ParseBomb;
  parsers[3] = &LoadMap::ParseBonus;
  parsers[4] = &LoadMap::ParsePlayer;
  parsers[5] = &LoadMap::ParseIa;

  while (i < 6 && cmd.find(type[i]) == std::string::npos)
    i++;
  if (i == 6)
  {
    std::cout << "Invalid object : '" << words.str() << "' at line " << line << std::endl;
    return (-1);
  }
  (*this.*parsers[i])(words, line);
  return (0);
}

int LoadMap::checkspace(int xbis, int ybis, int dir)
{
  std::list<Wall>::iterator itwall;
  int nbnearv = 2;
  int nbnearh = 2;

  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis - 1 && itwall->getPosY() == ybis); itwall++);
  if (itwall != _Mur->end())
    nbnearh--;
  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis + 1 && itwall->getPosY() == ybis); itwall++);
  if (itwall != _Mur->end())
    nbnearh--;
  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis && itwall->getPosY() == ybis - 1); itwall++);
  if (itwall != _Mur->end())
    nbnearv--;
  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis && itwall->getPosY() == ybis + 1); itwall++);
  if (itwall != _Mur->end())
    nbnearv--;
  if ((nbnearh + nbnearv < 2) || (nbnearh < 1 && dir == HOR) || (nbnearv < 1 && dir == VER))
    return (0);
  else
    return (1);
}

std::vector<t_pospos> LoadMap::searchPosposibility(int x, int y)
{
  std::vector<t_pospos> listpos;
  std::list<Wall>::iterator itwall;
  t_pospos  structpos;
  int i = 0;
  int j;

  while (i <= y)
  {
    j = 0;
    while (j <= x)
    {
      for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == j + 1 && itwall->getPosY() == i + 1); itwall++);
      if (checkspace(j + 1, i + 1, HOR) == 1 && checkspace(j + 1, i + 1, VER) == 1 && itwall == _Mur->end())
      {
        structpos.x = i + 1;
        structpos.y = j + 1;
        listpos.push_back(structpos);
      }
      j += 2;
    }
    i += 2;
  }
  return (listpos);
}

int LoadMap::read_map(std::istream &stream, Bomberman *bomberman, std::list<structPlayer> player, std::list<structIA> IA)
{
  char str[1048];
  int  nb;
  int  i = 0;
  int  line = 0;
  std::string string;
  std::istringstream words;

  if (stream.eof() == true)
  {
    // ERROR
    std::cout << "ERROR PARSING at line  "<< line << std::endl;
    return (-1);
  }
  stream.getline(str, 1048);
  line++;
  while (i < 2)
  {
    str[0] = 0;
    string = "";
    nb = 0;
    if (stream.eof() == true)
    {
      // ERROR
      std::cout << "ERROR PARSING at line  "<< line << std::endl;
      return (-1);
    }
    stream.getline(str, 1048);
    line++;
    words.clear();
    words.str(str);
    words >> string;
    if (string.find("Size") != std::string::npos)
    {
      words >> string;
      if (string.find("X") != std::string::npos)
      {
        words >> nb;
        bomberman->setSizeX(nb);
      }
      else if (string.find("Y") != std::string::npos)
      {
        words >> nb;
        bomberman->setSizeY(nb);
      }
    }
    i++;
  }
  while (!stream.eof())
  {
    str[0] = 0;
    string = "";
    stream.getline(str, 1048);
    line++;
    if (str[0] != '\n' && str[0] != 0)
    {
      words.clear();
      words.str(str);
      words >> string;
      lexCmd(string, words, line);
    }
  }
  placeplayers(searchPosposibility(bomberman->getSizeX(), bomberman->getSizeY()), player, IA);
  return (0);
}

int LoadMap::read_save(std::istream &stream, Bomberman *bomberman)
{
  char str[1048];
  int  nb;
  int  i = 0;
  int  line = 0;
  std::string string;
  std::istringstream words;

  while (i < 2)
  {
    str[0] = 0;
    string = "";
    nb = 0;
    if (stream.eof() == true)
    {
      // ERROR
      std::cout << "ERROR PARSING at line  "<< line << std::endl;
      return (-1);
    }
    stream.getline(str, 1048);
    line++;
    words.clear();
    words.str(str);
    words >> string;
    if (string.find("Size") != std::string::npos)
    {
      words >> string;
      if (string.find("X") != std::string::npos)
      {
        words >> nb;
        bomberman->setSizeX(nb);
      }
      else if (string.find("Y") != std::string::npos)
      {
        words >> nb;
        bomberman->setSizeY(nb);
      }
    }
    i++;
  }
  while (!stream.eof())
  {
    str[0] = 0;
    string = "";
    stream.getline(str, 1048);
    line++;
    if (str[0] != '\n' && str[0] != 0)
    {
      words.clear();
      words.str(str);
      words >> string;
      lexCmd(string, words, line);
    }
  }
  return (0);
}
