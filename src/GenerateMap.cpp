//
// GenerateMap.cpp for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/GenerateMap/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Mon May  4 16:59:21 2015 Victor Ganter
// Last update Sat Jun 13 16:16:26 2015 Victor Ganter
//

#include "../inc/GenerateMap.hh"
#include <unistd.h>

GenerateMap::GenerateMap(Bomberman *bomberman)
{
  _bomberman = bomberman;
  _Bombe = bomberman->getBomb();
  _Players = bomberman->getPlayer();
  _IA = bomberman->getIA();
  _Mur = bomberman->getWall();
  _Bonus = bomberman->getBonus();
}

GenerateMap::GenerateMap(Bomberman *bomberman, std::list<structPlayer> player, std::list<structIA> ia)
{
  _bomberman = bomberman;
  _Bombe = bomberman->getBomb();
  _Players = bomberman->getPlayer();
  _IA = bomberman->getIA();
  _Mur = bomberman->getWall();
  _Bonus = bomberman->getBonus();
  generate(bomberman->getSizeX(), bomberman->getSizeY(), player, ia);
}

GenerateMap::~GenerateMap()
{
}

void GenerateMap::add_cassable(int x, int y)
{
  _Mur->emplace_back(x, y, _bomberman, true);
}

void GenerateMap::add_incassable(int x, int y)
{
  _Mur->emplace_back(x, y, _bomberman, false);
}

void GenerateMap::add_player(int x, int y, std::string name, int id)
{
  _Players->emplace_back(x, y, name, id);
}

void GenerateMap::add_player(int x, int y, std::string name, int id, int range, int capacity, int score)
{
  _Players->emplace_back(x, y, name, id, range, capacity, score);
}

void GenerateMap::add_IA(int x, int y, std::string name, difficulty diff, int id)
{
  _IA->emplace_back(x, y, name, static_cast<enum difficulty>(diff), id, _bomberman);
}


void GenerateMap::add_IA(int x, int y, std::string name, difficulty diff, int id, int range, int capacity, int score)
{
  _IA->emplace_back(x, y, name, static_cast<enum difficulty>(diff), id, _bomberman, range, capacity, score);
}

void GenerateMap::add_Bonus(int x, int y, BonusType type)
{
  _Bonus->emplace_back(x, y, type);
}

void GenerateMap::add_Bomb(int x, int y, int range, int playerId, int time)
{
  _Bombe->emplace_back(x, y, range, playerId, time);
}

std::vector<t_pospos> GenerateMap::generatepospos(int length, int height)
{
  std::vector<t_pospos> listpos;
  t_pospos  structpos;
  int                   i = 0;
  int                   j = 0;

  while (j < height)
  {
    structpos.x = i + 1;
    structpos.y = j + 1;
    listpos.push_back(structpos);
    i += 2;
    if (i + 1 >= length)
    {
      j += 2;
      i = 2;
    }
  }
  return (listpos);
}

int GenerateMap::placeplayers(std::vector<t_pospos> listpos, std::list<structPlayer> player, std::list<structIA> ia)
{
  int i = 0;
  srand (time(NULL));
  std::vector<t_pospos>::iterator it;
  if (ia.size() + player.size() > listpos.size())
    throw MapError("map too small for the number of player");

  while (player.size() > 0)
  {
    it = listpos.begin();
    i = rand() % listpos.size();
    add_player(listpos[i].x, listpos[i].y, player.front().name, player.front().id);
    player.pop_front();
    while (i > 0)
    {
      it++;
      i--;
    }
    listpos.erase(it);
  }
  while (ia.size() > 0)
  {
    it = listpos.begin();
    i = rand() % listpos.size();
    add_IA(listpos[i].x, listpos[i].y, ia.front().name, ia.front().difficulty, ia.front().id);
    ia.pop_front();
    while (i > 0)
    {
      it++;
      i--;
    }
    listpos.erase(it);
  }
  return (0);
}

int GenerateMap::checknearplayer(int xbis, int ybis, int dir)
{
  std::list<Wall>::iterator itwall;
  int nbnearv = 2;
  int nbnearh = 2;

  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis - 1 && itwall->getPosY() == ybis); itwall++);
  if (itwall != _Mur->end())
    nbnearh--;
  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis + 1 && itwall->getPosY() == ybis); itwall++);
  if (itwall != _Mur->end())
    nbnearh--;
  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis && itwall->getPosY() == ybis - 1); itwall++);
  if (itwall != _Mur->end())
    nbnearv--;
  for (itwall = _Mur->begin(); itwall != _Mur->end() && !(itwall->getPosX() == xbis && itwall->getPosY() == ybis + 1); itwall++);
  if (itwall != _Mur->end())
    nbnearv--;
  if ((nbnearh + nbnearv <= 2) || (nbnearh <= 1 && dir == HOR) || (nbnearv <= 1 && dir == VER))
    return (0);
  else
    return (1);
}

int GenerateMap::searchnearplayer(int x, int y, int dir)
{
  std::list<Player>::iterator it;
  std::list<IA>::iterator itia;
  int xbis;
  int ybis;

  it = _Players->begin();
  while (it != _Players->end())
  {
    while (it != _Players->end() && !(it->getPosX() == x && it->getPosY() == y))
      it++;
    if (it != _Players->end())
    {
      xbis = it->getPosX();
      ybis = it->getPosY();
      if (checknearplayer(xbis, ybis, dir) == 0)
        return (1);
      it++;
    }
  }
  itia = _IA->begin();
  while (itia != _IA->end())
  {
    while (itia != _IA->end() && !(itia->getPosX() == x && itia->getPosY() == y))
      itia++;
    if (itia != _IA->end())
    {
      xbis = itia->getPosX();
      ybis = itia->getPosY();
      if (checknearplayer(xbis, ybis, dir) == 0)
        return (1);
      itia++;
    }
  }
  return (0);
}

int GenerateMap::randwall(int x, int y)
{
  int i = 0;
  std::list<Player>::iterator it;
  std::list<IA>::iterator itia;

  for (it = _Players->begin(); it != _Players->end() && !(it->getPosX() == x && it->getPosY() == y); it++);
  for (itia = _IA->begin(); itia != _IA->end() && !(itia->getPosX() == x && itia->getPosY() == y); itia++);
  if (it != _Players->end() || itia != _IA->end())
    return (0);
  if (searchnearplayer(x - 1, y, HOR) == 0 && searchnearplayer(x + 1, y, HOR) == 0 && searchnearplayer(x, y - 1, VER) == 0 && searchnearplayer(x, y + 1, VER) == 0)
  {
    i = rand() % 6;
    if (i <= 1)
      return (0);
    else if (i <= 4)
      return (1);
    else
      return (2);
  }
  else
    return (0);
}

int GenerateMap::generate(int x, int y, std::list<structPlayer> player, std::list<structIA> IAlt)
{
  int i = 0;
  int j = 0;
  int k = 0;
  int xext = x + 2;
  int yext = y + 2;

  if (static_cast<unsigned long>((static_cast<float>(x) / 2) * (static_cast<float>(y) / 2)) < (player.size() + IAlt.size()))
    throw MapError("map too small for the number of player");
  else
    placeplayers(generatepospos(x, y), player, IAlt);
  while (j == 0)
  {
    add_incassable(i, j);
    if (i == (xext - 1))
    {
      j++;
      i = -1;
    }
    i++;
  }
  while (j < (y + 1))
  {
    if (i == 0)
      add_incassable(i, j);
    else if (i == (x + 1))
    {
      add_incassable(i, j);
      j++;
      i = -1;
    }
    i++;
  }
  i = 0;
  while (j < yext)
  {
    add_incassable(i, j);
    if (i == (xext - 1))
    {
      j++;
      i = -1;
    }
    i++;
  }

  i = 1;
  j = 1;
  while (j <= y)
  {
    k = randwall(i, j);
    if (k == 1)
      add_cassable(i, j);
    else if (k == 2)
      add_incassable(i, j);
    if (i == x)
    {
      j++;
      i = 0;
    }
    i++;
  }
  return (0);
}
