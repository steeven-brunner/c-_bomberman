//
// CondVar.cpp for CondVar in /home/gaessl_m/rendu/cpp_plazza/Kitchen
//
// Made by Marin Gaessler
// Login   <gaessl_m@epitech.net>
//
// Started on  Mon Apr 20 16:05:44 2015 Marin Gaessler
// Last update Mon Apr 20 16:41:43 2015 Marin Gaessler
//

#include "CondVar.hh"

CondVar::CondVar()
{
}

CondVar::CondVar(Mutex *mutex)
  : _mutex(mutex)
{
  if (pthread_cond_init(&_cond, NULL) != 0)
    throw ThreadError("Failed pthread_cond_init");
}

CondVar::~CondVar()
{
  if (pthread_cond_destroy(&_cond) != 0)
    throw ThreadError("Failed pthread_cond_destroy");
}

void  CondVar::init(Mutex *mutex)
{
  _mutex = mutex;
  if (pthread_cond_init(&_cond, NULL) != 0)
    throw ThreadError("Failed pthread_cond_init");
}

void	CondVar::wait()
{
  if ((pthread_cond_wait(&_cond, _mutex->getMutex())) != 0)
    throw ThreadError("Failed pthread_cond_wait");
}

void	CondVar::signal()
{
  if ((pthread_cond_signal(&_cond)) != 0)
    throw ThreadError("Failed pthread_cond_signal");
}

void	CondVar::broadcast()
{
  if ((pthread_cond_broadcast(&_cond)) != 0)
    throw ThreadError("Failed pthread_cond_broadcast");
}
