//
// GameEngine.cpp for GameEngine in /home/dayrie_l/rendu/cpp_bomberman/graphic
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon May 11 12:26:19 2015 Lucas Dayries
// Last update Sun Jun 14 20:16:05 2015 Steeven Brunner
//

#include "GameEngine.hpp"
#include "Bomberman.hh"

GameEngine::GameEngine(Bomberman *bomb, bool multi, int x, int y)
{
  _bomb = bomb;
  bombNb = 0;
  bombNbErase = 0;
  _x = 0;
  _y = _bomb->getSizeX() + 10;
  _z = -10;
  _multi = multi;
  _sizeX = x;
  _sizeY = y;
  _p1Alive = true;
  _p2Alive = true;
}

GameEngine::~GameEngine()
{
  for (size_t i = 0; i < _floors.size(); ++i)
    delete _floors[i];
  for (size_t i = 0; i < _walls.size(); ++i)
    delete _walls[i];
}

bool GameEngine::initialize()
{
  _y = _bomb->getSizeX() + 10;
  int x = 0;
  if (!_context.start(_sizeX, _sizeY, "My bomberman!"))
    return false;
  glEnable(GL_DEPTH_TEST);
  if (!_shader.load("shaders/basic.fp", GL_FRAGMENT_SHADER)
      || !_shader.load("shaders/basic.vp", GL_VERTEX_SHADER)
      || !_shader.build())
    return false;
  Texture *tex = new Texture;
  if (tex->tex == false)
    return (false);
  _projection = glm::perspective(60.0f, static_cast<float>(_sizeX) / static_cast<float>(_sizeY), 0.1f, 100.0f);
  _transformation = glm::lookAt(glm::vec3(_x, _y, _z), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
  _shader.bind();
  _shader.setUniform("view", _transformation);
  _shader.setUniform("projection", _projection);
  _shader.setUniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
  AObject *skymap = new SkyMap(tex);
  _skymap = skymap;
  _skymap->initialize();
  _skymap->translate(glm::vec3(_bomb->getSizeX() / 2, 0, _bomb->getSizeY() / 2));
  _skymap->scale(glm::vec3(_bomb->getSizeX() * 15, _bomb->getSizeX(), _bomb->getSizeX() * 15));
  while (x != (_bomb->getSizeX() + 1) * (_bomb->getSizeY() + 1))
  {
    AObject *floorx = new Floor(tex);
    if (floorx->initialize() == false)
      return (false);
    _floors.push_back(floorx);
    _floors[x]->translate(glm::vec3(x % (_bomb->getSizeX() + 1), 0, x / (_bomb->getSizeX() + 1)));
    x++;
  }
  x = 0;
  for (std::list<Wall>::iterator it = _bomb->getWall()->begin(); it != _bomb->getWall()->end(); ++it)
  {
    AObject *cubex;
    if (it->getIsBreakable())
      cubex = new Cube(tex, &(*it));
    else
      cubex = new SolidCube(tex, &(*it));
    if (!cubex->initialize())
      return (false);
    _walls.push_back(cubex);
    _walls[x]->translate(glm::vec3(std::next(_bomb->getWall()->begin(), x)->getPosX(), 0, std::next(_bomb->getWall()->begin(), x)->getPosY()));
    x++;
  }
  for (std::list<Player>::iterator it = _bomb->getPlayer()->begin(); it != _bomb->getPlayer()->end(); ++it)
  {
    ACharacter *charx = new Character(&(*it));
    if (it == _bomb->getPlayer()->begin())
      dynamic_cast<Character*>(charx)->setColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    else
      dynamic_cast<Character*>(charx)->setColor(glm::vec4((rand()) / static_cast <float> (RAND_MAX), (rand()) / static_cast <float> (RAND_MAX), (rand()) / static_cast <float> (RAND_MAX), 1.0f));
    if (charx->initialize() == false)
      return (false);
    _characters.push_back(charx);
  }
  for (std::list<IA>::iterator it = _bomb->getIA()->begin(); it != _bomb->getIA()->end(); ++it)
  {
    ACharacter *charx = new Character(&(*it));
    dynamic_cast<Character*>(charx)->setColor(glm::vec4((rand()) / static_cast <float> (RAND_MAX), (rand()) / static_cast <float> (RAND_MAX), (rand()) / static_cast <float> (RAND_MAX), 1.0f));
    if (charx->initialize() == false)
      return (false);
    _characters.push_back(charx);
  }
  for (int z = 0; z != 50; ++z)
  {
    AObject *boomb = new BombGraph(tex);
    if (boomb->initialize() == false)
      return (false);
    boomb->setDrop(false);
    boomb->scale(glm::vec3(0.002, 0.002, 0.002));
    _bombs.push_back(boomb);
  }
  for (int z = 0; z != _bomb->getSizeX() * _bomb->getSizeY(); z++)
  {
    Explosion *newexplo = new Explosion(tex);
    if (newexplo->initialize() == false)
      return (false);
    _explosions.push_back(newexplo);
  }
  for (size_t z = 0; z != _bomb->getWall()->size() - ((_bomb->getSizeX() / 2) + 2 + (_bomb->getSizeY() / 2)); z++)
  {
    BonusGraph *newbonus = new BonusGraph(tex->getTexBonusBomb(), tex->getTexBonusFire(), tex->getTexBonusSpeed());
    if (newbonus->initialize() == false)
      return (false);
    _bonus.push_back(newbonus);
  }
  return (true);
}

bool        GameEngine::update()
{
  if (_input.getKey(SDLK_ESCAPE) || _input.getInput(SDL_QUIT))
  {
    _context.stop();
    return (false);
  }
  if (_input.getKey(SDLK_KP_PLUS))
    _y -= _y <= 10 ? 0 : 1;
  if (_input.getKey(SDLK_KP_MINUS))
    _y += _y >= _bomb->getSizeX() + 10 ? 0 : 1;
  for (size_t i = 0; i < _floors.size(); ++i)
    _floors[i]->update(_clock, _input);
  for (size_t i = 0; i < _walls.size(); ++i)
      _walls[i]->update(_clock, _input);
  for (size_t i = 0; i < _walls.size(); ++i)
  {
    if (dynamic_cast<Cube*>(_walls[i]) != NULL && !dynamic_cast<Cube*>(_walls[i])->getWall()->isShow())
    {
      delete _walls[i];
      _walls.erase(_walls.begin() + i);
      _walls.shrink_to_fit();
    }
  }
  for (std::list<Wall>::iterator it = _bomb->getWall()->begin(); it != _bomb->getWall()->end(); ++it)
  {
    if (!it->isShow())
      it = _bomb->getWall()->erase(it);
  }
  for (size_t i = 0; i < _characters.size(); ++i)
  {
    _characters[i]->update(_clock, _input);
  }
  for (size_t i = 0; i < _characters.size(); ++i)
  {
    if (!dynamic_cast<Character*>(_characters[i])->getPlayer()->isAlive())
    {
      if (dynamic_cast<Character*>(_characters[i])->getPlayer()->getId() == 0)
        _p1Alive = false;
      if (dynamic_cast<Character*>(_characters[i])->getPlayer()->getId() == 1)
        _p2Alive = false;
      delete _characters[i];
      _characters.erase(_characters.begin() + i);
      _characters.shrink_to_fit();
    }
  }
  for (std::list<Player>::iterator it = _bomb->getPlayer()->begin(); it != _bomb->getPlayer()->end(); ++it)
  {
    if (!it->isAlive())
      it = _bomb->getPlayer()->erase(it);
  }
  for (std::list<IA>::iterator it = _bomb->getIA()->begin(); it != _bomb->getIA()->end(); ++it)
  {
    if (!it->isAlive())
      it = _bomb->getIA()->erase(it);
  }
  for (size_t i = 0; i < _bombs.size(); ++i)
    _bombs[i]->update(_clock, _input);
  _skymap->update(_clock, _input);
  _shader.setUniform("view", _transformation);
  _shader.setUniform("projection", _projection);
  return true;
}

void        GameEngine::drawGame()
{
  for (size_t i = 0; i < _characters.size(); ++i)
  {
    _shader.setUniform("color", dynamic_cast<Character*>(_characters[i])->getColor());
    _characters[i]->draw(_shader, _clock);
  }
  _shader.setUniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
  for (size_t i = 0; i < _floors.size(); ++i)
    _floors[i]->draw(_shader, _clock);
  for (size_t i = 0; i < _walls.size(); ++i)
    _walls[i]->draw(_shader, _clock);
  for (size_t i = 0; i < _bombs.size(); ++i)
  {
    if (_bombs[i]->getDrop() == true)
     _bombs[i]->draw(_shader, _clock);
  }
  for (size_t i = 0; i < _explosions.size(); ++i)
  {
    if (_explosions[i]->getPrint())
    {
      _explosions[i]->draw(_shader, _clock);
      dynamic_cast<Explosion*>(_explosions[i])->checkDestroy();
    }
  }
  for (size_t i = 0; i < _bonus.size(); ++i)
  {
    if (_bonus[i]->checkActive())
      _bonus[i]->draw(_shader, _clock);
  }
  _skymap->draw(_shader, _clock);
}

void        GameEngine::draw()
{
  if (!_multi)
    {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      _shader.bind();
      _transformation = glm::lookAt(_characters[0]->getPosition() + glm::vec3(_x, _y, _z), _characters[0]->getPosition(), glm::vec3(0, 1, 0));
      drawGame();
      _context.flush();
    }
  else
    {
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      _shader.bind();
      glViewport( - _sizeX / 4, 0, _sizeX, _sizeY);
      glScissor(0, 0, _sizeX / 2, _sizeY);
      glEnable(GL_SCISSOR_TEST);
      if (_p1Alive && _p2Alive)
        _transformation = glm::lookAt(_characters[1]->getPosition() + glm::vec3(_x, _y, _z), _characters[1]->getPosition(), glm::vec3(0, 1, 0));
      else
      _transformation = glm::lookAt(_characters[0]->getPosition() + glm::vec3(_x, _y, _z), _characters[0]->getPosition(), glm::vec3(0, 1, 0));

      drawGame();
      _shader.setUniform("view", _transformation);
      glDisable(GL_SCISSOR_TEST);

      glViewport(_sizeX / 4, 0, _sizeX, _sizeY);
      glScissor(_sizeX / 2, 0, _sizeX / 2, _sizeY);
      glEnable(GL_SCISSOR_TEST);
      _transformation = glm::lookAt(_characters[0]->getPosition() + glm::vec3(_x, _y, _z), _characters[0]->getPosition(), glm::vec3(0, 1, 0));
      drawGame();
      _shader.setUniform("view", _transformation);
      glDisable(GL_SCISSOR_TEST);
      _context.flush();
     }
}

std::vector<AObject*> *GameEngine::getBombs()
{
  return (&_bombs);
}

std::vector<Explosion*> *GameEngine::getExplosions()
{
  return (&_explosions);
}

std::vector<BonusGraph*> *GameEngine::getBonus()
{
  return (&_bonus);
}

gdl::SdlContext &GameEngine::getContext()
{
  return (_context);
}

gdl::Input *GameEngine::getInput()
{
  _context.updateClock(_clock);
  _context.updateInputs(_input);
  return (&_input);
}

void GameEngine::moveCharacter(int id, move dir, float speed)
{
  for (std::vector<ACharacter *>::iterator it = _characters.begin(); it != _characters.end(); ++it)
    {
      if ((dynamic_cast<Character *>(*it))->getPlayer()->getId() == id)
      {
        dynamic_cast<Character *>((*it))->moveCharacter(_bomb, _clock, dir, speed);
      }
    }
}

int GameEngine::getBombNb()
{
  return (bombNb);
}

void GameEngine::incrBombNb()
{
  if (bombNb >= 49)
    bombNb = 0;
  else
    bombNb++;
}

int GameEngine::getBombNbErase()
{
  return (bombNbErase);
}

void GameEngine::incrBombNbErase()
{
  if (bombNbErase >= 49)
    bombNbErase = 0;
  else
    bombNbErase++;
}

void        GameEngine::setMulti(bool multi)
{
  _multi = multi;
}

void GameEngine::dropBomb(float x, float y)
{
  AObject *bomb = new BombGraph;
  if (bomb->initialize() == false)
    return ;
  bomb->scale(glm::vec3(0.025, 0.025, 0.025));
  bomb->translate(glm::vec3(x, 0, y));
  _bombs.push_back(bomb);
}
