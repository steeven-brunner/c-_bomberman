//
// AObject.cpp for AObject in /home/dayrie_l/rendu/cpp_bomberman/graphic
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon May 11 12:16:16 2015 Lucas Dayries
// Last update Sat Jun 13 13:04:43 2015 Steeven Brunner
//

#include "AObject.hpp"

AObject::AObject()
  : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1)
{
}

AObject::~AObject()
{
}

bool	AObject::initialize()
{
  return (true);
}

void	AObject::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
}

void	AObject::translate(glm::vec3 const &v)
{
  _position += v;
}

void	AObject::rotate(glm::vec3 const& axis, float angle)
{
  _rotation += axis * angle;
}

void	AObject::scale(glm::vec3 const& scale)
{
  _scale *= scale;
}

void AObject::setDrop(bool set)
{
  _drop = set;
}

glm::vec3 AObject::getPos() const
{
  return (_position);
}

bool AObject::getDrop()
{
  return (_drop);
}

glm::mat4	AObject::getTransformation()
{
  glm::mat4 transform(1);
  transform = glm::translate(transform, _position);
  transform = glm::scale(transform, _scale);
  transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
  transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
  transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
  return (transform);
}
