//
// GameOver.cpp for bomberman in /usr/media/suse/ganter_v/rendu/cpp_bomberman/src/
//
// Made by Victor Ganter
// Login   <ganter_v@epitech.eu>
//
// Started on  Fri Jun 12 14:36:18 2015 Victor Ganter
// Last update Sun Jun 14 16:13:33 2015 Steeven Brunner
//

#include "GameOver.hh"

GameOver::GameOver(int x, int y, int idwin): _x(x), _y(y), _idwin(idwin)
{
  SDL_Rect	size;

  size.x = 0;
  size.y = 0;
  size.h = y;
  size.w = x;
  _window = SDL_CreateWindow("Bomberman", 0, 0, x, y, 0);
  if ((_police = TTF_OpenFont("fonts/Super Mario Bros.ttf", 500)) == NULL)
    std::cout << TTF_GetError() << std::endl;
  SDL_GetCurrentDisplayMode(0, &_current);
  if (_current.w == x && _current.h == y)
    SDL_SetWindowFullscreen(_window, SDL_WINDOW_FULLSCREEN);
  _renderer = SDL_CreateRenderer(_window, 0, SDL_RENDERER_ACCELERATED
        | SDL_RENDERER_TARGETTEXTURE
        | SDL_RENDERER_PRESENTVSYNC);
  SDL_RenderClear(_renderer);
  if (_renderer == NULL)
  {
    SDL_DestroyWindow(_window);
    std::cout << "SDL_CreateRenderer Error" << std::endl;
    SDL_Quit();
  }
  if (idwin < 0)
  {
    SDL_Surface* _surface = SDL_CreateRGBSurface(0, x, y, 32, 0, 0, 0, 0);
    _gameover = IMG_Load("./textures/gameover.png");
    if (_gameover == NULL)
      std::cout << "error loading img" << std::endl;
    SDL_BlitScaled(_gameover, NULL, _surface, &size);
    _texture = SDL_CreateTextureFromSurface(_renderer, _surface);
    SDL_RenderCopy(_renderer, _texture, NULL, NULL);
    SDL_RenderPresent(_renderer);
    if (_texture != NULL)
      SDL_DestroyTexture(_texture);

    SDL_Color colortext = {0, 0, 0, 0};
    SDL_Surface* surfaceMessage = TTF_RenderText_Solid(_police, "Game Over", colortext);
    SDL_Texture* Message = SDL_CreateTextureFromSurface(_renderer, surfaceMessage);
    SDL_Rect Message_rect;
    Message_rect.x = x / 4;
    Message_rect.y = y / 4;
    Message_rect.w = x / 2;
    Message_rect.h = y / 4;
    SDL_RenderCopy(_renderer, Message, NULL, &Message_rect);
    SDL_RenderPresent(_renderer);
    SDL_FreeSurface(surfaceMessage);
    SDL_DestroyTexture(Message);
  }
  else
    printwin();
}

GameOver::~GameOver()
{
  SDL_DestroyRenderer(_renderer);
  SDL_DestroyWindow(_window);
}

std::string GameOver::inputName()
{
  std::string text;
  bool done = false;
  bool renderText;
  SDL_Color colortext = {0, 0, 0, 0};
  SDL_Rect Message_rect;
  Message_rect.x = _x / 20 * 2;
  Message_rect.y = _y / 8 * 4;
  Message_rect.h = _y / 8;

  SDL_StartTextInput();
  while (!done)
  {
    SDL_Event event;
    if (SDL_PollEvent(&event))
    {
      if (event.type == SDL_QUIT)
        done = true;
      if( event.type == SDL_KEYDOWN )
      {
        if(event.key.keysym.sym == SDLK_BACKSPACE && text.length() > 0 )
        {
          text.pop_back();
          renderText = true;
        }
        else if (event.key.keysym.sym == SDLK_RETURN)
          done = true;
        else if (event.type == SDLK_ESCAPE)
        {
          done = true;
          text = "";
        }
      }
      else if(event.type == SDL_TEXTINPUT)
      {
        if(!((event.text.text[ 0 ] == 'c' || event.text.text[ 0 ] == 'C') && ( event.text.text[ 0 ] == 'v' || event.text.text[ 0 ] == 'V') && SDL_GetModState() & KMOD_CTRL) && text.size() <= 16)
        {
            text += event.text.text;
            renderText = true;
        }
      }
    }
    if(renderText)
    {
      Message_rect.w = _x;
      SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
      SDL_RenderFillRect(_renderer, &Message_rect);
      Message_rect.w = (_x / 20) * (text.size());
      Message_rect.x = (_x / 20) * (10 - text.size() / 2);
      SDL_Surface* surfaceMessage = TTF_RenderText_Solid(_police, text.c_str(), colortext);
      SDL_Texture* Message = SDL_CreateTextureFromSurface(_renderer, surfaceMessage);
      SDL_RenderCopy(_renderer, Message, NULL, &Message_rect);
      SDL_RenderPresent(_renderer);
      SDL_DestroyTexture(Message);
      renderText = false;
    }
  }
  SDL_StopTextInput();
  return (text);
}

void GameOver::printwin()
{
  std::stringstream win;
  SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
  SDL_Rect rectangle;
  rectangle.x = 0;
  rectangle.y = 0;
  rectangle.w = _x;
  rectangle.h = _y;
  SDL_RenderFillRect(_renderer, &rectangle);
  SDL_RenderPresent(_renderer);

  win << "Player " << _idwin + 1 << " wins";
  SDL_Color colortext = {0, 0, 0, 0};
  SDL_Surface* surfaceMessage = TTF_RenderText_Solid(_police, win.str().c_str(), colortext);
  SDL_Texture* Message = SDL_CreateTextureFromSurface(_renderer, surfaceMessage);
  SDL_Rect Message_rect;
  Message_rect.x = _x / 4;
  Message_rect.y = _y / 6;
  Message_rect.w = _x / 2;
  Message_rect.h = _y / 6;
  SDL_RenderCopy(_renderer, Message, NULL, &Message_rect);
  win.str("");
  win << "Enter Name :";
  surfaceMessage = TTF_RenderText_Solid(_police, win.str().c_str(), colortext);
  Message = SDL_CreateTextureFromSurface(_renderer, surfaceMessage);
  Message_rect.x = _x / 4;
  Message_rect.y = _y / 6 * 2;
  Message_rect.w = _x / 2;
  Message_rect.h = _y / 6;
  SDL_RenderCopy(_renderer, Message, NULL, &Message_rect);
  SDL_RenderPresent(_renderer);
  SDL_FreeSurface(surfaceMessage);
  SDL_DestroyTexture(Message);
}
