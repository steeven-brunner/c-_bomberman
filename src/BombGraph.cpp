//
// BombGraph.cpp for BombGraph in /home/dayrie_l/rendu/cpp_bomberman/inc
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon Jun  8 11:21:36 2015 Lucas Dayries
// Last update Wed Jun 10 17:50:51 2015 Marin GAESSLER
//

#include "BombGraph.hpp"

BombGraph::BombGraph()
  : _drop(true)
{
}

BombGraph::BombGraph(Texture *tex)
  : _drop(true), _resize(1.05), _i(0)
{
  _texture = tex->getTexBomb();
  _position = glm::vec3(-0.5, -0.2, -0.5);
}

BombGraph::~BombGraph()
{
}

bool	BombGraph::initialize()
{
  if ((_model.load("lib_graphic/assets/bomb.fbx")) == false)
    {
      std::cerr << "Could not load 3D model." << std::endl;
      return (false);
    }
  return (true);
}

void	BombGraph::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
}

void	BombGraph::draw(gdl::BasicShader &shader, gdl::Clock const &clock)
{
  if (_drop == true)
  {
    scale(glm::vec3(_resize, _resize, _resize));
    if (_i == 10)
    {
      if (_resize > 1)
        _resize = 0.95;
      else if (_resize < 1)
        _resize = 1.05;
      _i = 0;
    }
    _i++;
    _texture->bind();
	  _model.draw(shader, getTransformation(), clock.getElapsed());
  }
  else
  {
    _i = 0;
  	return ;
  }
}

void BombGraph::reset()
{
	_position = glm::vec3(-0.5, -0.2, -0.5);
}
