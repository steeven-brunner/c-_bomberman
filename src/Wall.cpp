//
// Wall.cpp for Bomberman in /home/brunne_s/rendu/cpp_bomberman/Game
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Wed May  6 11:31:03 2015 Steeven Brunner
// Last update Sun Jun 14 16:24:25 2015 Steeven Brunner
//

#include <cstdlib>

#include "Wall.hh"
#include "Bonus.hh"
#include "Bomberman.hh"

Wall::Wall(float posX, float posY, Bomberman * bomberman, bool isBreakable)
  : HardObject(posX, posY), _isBreakable(isBreakable), _isShow(true)
{
  _bomberman = bomberman;
}

Wall::~Wall()
{
}

void	Wall::setIsBreakable(bool isBreakable)
{
  _isBreakable = isBreakable;
}

bool	Wall::getIsBreakable() const
{
  return (_isBreakable);
}

bool  Wall::isShow() const
{
  return (_isShow);
}

void  Wall::setShow(bool show)
{
  _isShow = show;
  std::cout << " wall deleted " << std::endl;
}

void  Wall::breakWall(std::vector<BonusGraph*> *bonus)
{
  static int save_type = 0;
  int			r;
  int			type;
  size_t		i = 0;
  BonusType		bonus_type;
  std::list<Bonus>	*list_bonus = _bomberman->getBonus();

  _isShow = false;
  if (_isBreakable == true)
    {
      r = rand() % 3;
      type = rand() % SIZE_BONUS;
      type = (type == save_type ? (type + 1) % SIZE_BONUS : type);
      if (r == 0)
	{
	  switch (type)
	    {
	    case 0:
	      save_type = 0;
	      bonus_type = MORE_BOMB;
	      break;
	    case 1:
	      save_type = 1;
	      bonus_type = FASTER;
	      break;
	    case 2:
	      save_type = 2;
	      bonus_type = MORE_RANGE;
	      break;
	    }
	  list_bonus->emplace_back(_posX, _posY, bonus_type);
    for (i = 0; i < bonus->size() && bonus->at(i)->checkActive(); i++);
    if (i != bonus->size())
      bonus->at(i)->setActive(&list_bonus->back(), _posX, _posY, type);
	}
    }
}
