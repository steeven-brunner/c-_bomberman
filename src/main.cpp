//
// main.cpp for bomberman in /home/schler_e/Projet Epitech/C++/cpp_bomberman/src/
//
// Made by Edwin SCHLERET
// Login   <schler_e@epitech.eu>
//
// Started on  Mon May 18 12:58:36 2015 Edwin SCHLERET
// Last update Sun Jun 14 17:50:37 2015 Marin Gaessler
//

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "Menu.hh"

int     main()
{
  Menu 	m(1600, 900);

  if (m.init() != 0)
    return (-1);
  m.intro();
  m.mainMenu();
  return (0);
}
