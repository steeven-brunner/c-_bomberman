//
// Texture.cpp for Texture in /home/dayrie_l/rendu/cpp_bomberman/src
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Fri May 22 13:56:58 2015 Lucas Dayries
// Last update Thu Jun 11 18:24:44 2015 Steeven Brunner
//

#include "Texture.hpp"

Texture::Texture()
{
  textures texAssign = &Texture::assignTexture;
  (*this.*texAssign)(&_textureWall, "textures/WallBreak.tga");
  (*this.*texAssign)(&_textureFloor, "textures/Floor.tga");
  (*this.*texAssign)(&_textureSolid, "textures/WallSolid.tga");
  (*this.*texAssign)(&_textureSkyMap, "textures/SkyBox/Skybox_TOP.tga");
  (*this.*texAssign)(&_textureExplosion, "textures/Explosion.tga");
  (*this.*texAssign)(&_textureBomb, "lib_graphic/assets/bomb.fbm/Bomb_texture.tga");
  (*this.*texAssign)(&_textureBonusFire, "textures/Bonus/Fire.tga");
  (*this.*texAssign)(&_textureBonusBomb, "textures/Bonus/Bomb.tga");
  (*this.*texAssign)(&_textureBonusSpeed, "textures/Bonus/Speed.tga");
  tex = true;
}

Texture::~Texture()
{
}

void Texture::assignTexture(gdl::Texture *texture, std::string texName)
{
  if (texture->load(texName) == false)
  {
    std::cerr << "Cannot load the texture." << std::endl;
    tex = false;
  }
}

gdl::Texture *Texture::getTexWall()
{
  return (&_textureWall);
}

gdl::Texture *Texture::getTexSolid()
{
  return (&_textureSolid);
}

gdl::Texture *Texture::getTexFloor()
{
  return (&_textureFloor);
}

gdl::Texture *Texture::getTexSkyMap()
{
  return (&_textureSkyMap);
}

gdl::Texture *Texture::getTexExplosion()
{
  return (&_textureExplosion);
}

gdl::Texture *Texture::getTexBomb()
{
  return (&_textureBomb);
}

gdl::Texture *Texture::getTexBonusFire()
{
  return (&_textureBonusFire);
}

gdl::Texture *Texture::getTexBonusBomb()
{
  return (&_textureBonusBomb);
}

gdl::Texture *Texture::getTexBonusSpeed()
{
  return (&_textureBonusSpeed);
}
