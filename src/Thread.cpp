//
// Thread.cpp for Thread in /home/gaessl_m/rendu/cpp_plazza/tp
//
// Made by Marin Gaessler
// Login   <gaessl_m@epitech.net>
//
// Started on  Thu Apr 16 15:14:54 2015 Marin Gaessler
// Last update Thu Apr 16 15:23:10 2015 Marin Gaessler
//

#include <iostream>

#include "Thread.hh"

Thread::Thread()
{
  _status = NotCreated;
}

Thread::~Thread()
{
}

int		Thread::create(pFct f, void *arg)
{
  if (_status == Running)
    return (-2);
  if ((pthread_create(&_thread, NULL, f, arg)) != 0)
    throw ThreadError("Failed pthread_create");
  _status = Running;
  return (0);
}

int		Thread::join()
{
  if (_status != Running)
    return (-2);
  if ((pthread_join(_thread, NULL)) != 0)
    throw ThreadError("Failed pthread_join");
  _status = Dead;
  return (0);
}

void		Thread::kill(int sig)
{
  if ((pthread_kill(_thread, sig)) != 0)
    throw ThreadError("Failed pthread_kill");
}

Status		Thread::getStatus() const
{
  return (_status);
}
