//
// SoftObject.cpp for Bomberman in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 16:13:58 2015 Steeven Brunner
// Last update Mon May  4 16:28:00 2015 Steeven Brunner
//

#include "SoftObject.hh"

SoftObject::SoftObject(float posX, float posY)
  : Object(posX, posY, false)
{
}

SoftObject::~SoftObject()
{
}
