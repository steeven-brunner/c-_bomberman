//
// Score.cpp for Score.cpp in /home/brunne_s/rendu/cpp_bomberman/src
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Thu May 21 17:35:20 2015 Steeven Brunner
// Last update Sun Jun 14 15:01:38 2015 Steeven Brunner
//

#include "Score.hh"
#include "Bomberman.hh"

Score::Score()
{

}

Score::~Score()
{

}

void		Score::saveScore(Bomberman & bomberman)
{
  std::list<Player>	*player = bomberman.getPlayer();
  
  std::ofstream   score_file("score_file.txt", std::ios::in | std::ios::app);

  if (score_file)
    {
      for (std::list<Player>::iterator itPlayer = player->begin(); itPlayer != player->end(); ++itPlayer)
	score_file << (*itPlayer).getName() << ":" << (*itPlayer).getScore() << "\n";
      score_file.close();
    }
  else
    std::cout << "Impossible to open [score_file.txt]" << std::endl;
}


std::list<std::tuple<std::string, int> >	Score::loadScore()
{
  std::ifstream                                 myfile("score_file.txt");
  std::string                                   line;
  std::string                                   player;
  int                                           score;
  std::list<std::tuple<std::string, int> >      my_list;

  if (!myfile.is_open())
    {
      std::cout << "Can not open the file" << std::endl;
      return (my_list);
    }
  while (getline(myfile, player, ':'))
    {
      getline(myfile, line, '\n');
      score = std::stoi(line);
      line.clear();
      my_list.emplace_back(player, score);
    }
  myfile.close();
  return (my_list);
}

std::list<std::tuple<std::string, int> >  Score::getTop(std::list<std::tuple<std::string, int> > list, size_t listSize)
{
  std::tuple<std::string, int> save;
  std::list<std::tuple<std::string, int> >::iterator it2;
  std::list<std::tuple<std::string, int> > tmp;

  while (tmp.size() < listSize)
  {
    save = list.front();
    it2 = list.begin();
    for (std::list<std::tuple<std::string, int> >::iterator it = list.begin(); it != list.end(); ++it)
    {
      if (std::get<1>(*it) > std::get<1>(save))
      {
        save = (*it);
        it2 = it;
      }
    }
    tmp.push_back(save);
    list.erase(it2);
  }
  return tmp;
}
