//
// Bomb.cpp for Bomb.cpp in /home/brunne_s/rendu/cpp_bomberman/Game
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Tue May  5 17:14:35 2015 Steeven Brunner
// Last update Sun Jun 14 16:02:58 2015 Steeven Brunner
//

#include "Bomberman.hh"
#include "Player.hh"
#include "Bomb.hh"

Bomb::Bomb(float posX, float posY, int range, int playerId, int time)
  : SoftObject(posX, posY), _range(range), _playerId(playerId), _time(time),
    _willExplose(false)
{
  _startTime = std::chrono::steady_clock::now();
}

Bomb::~Bomb()
{
}

int	Bomb::DestroyObj(Bomberman &bomberman, int posX, int posY, bool isPlayer,
			 Player &itPlayer, IA &itIA)
{
  std::list<Player>                   *player = bomberman.getPlayer();
  std::list<IA>                       *ia = bomberman.getIA();
  std::list<Wall>                     *wall = bomberman.getWall();
  std::list<Bomb>		      *bomb = bomberman.getBomb();

  std::list<Player>::iterator         itPlayerCheck = player->begin();
  std::list<IA>::iterator	      itIACheck = ia->begin();
  std::list<Wall>::iterator           itWall = wall->begin();
  std::list<Bomb>::iterator	      itBomb = bomb->begin();

  while (itPlayerCheck != player->end())
    {
      if (posX == static_cast<int>((*itPlayerCheck).getPosX()) && posY ==
	  static_cast<int>((*itPlayerCheck).getPosY()))
	{
	  itPlayerCheck->setAlive(false);
	  if (isPlayer == true)
	    {
	      itPlayer.setScore(10);
	      return (10);
	    }
	  else
	    {
	      itIA.setScore(10);
	      return (10);
	    }
	}
      ++itPlayerCheck;
    }
  while (itIACheck != ia->end())
    {
      if (posX == static_cast<int>((*itIACheck).getPosX()) && posY ==
	  static_cast<int>((*itIACheck).getPosY()))
	{
	  itIACheck->setAlive(false);
	  if (isPlayer == true)
	    {
	      itPlayer.setScore(10);
	      return (10);
	    }
	  else
	    {
	      itIA.setScore(10);
	      return (10);
	    }
	}
      ++itIACheck;
    }
  while (itWall != wall->end())
    {
      if (posX == (*itWall).getPosX() && posY == (*itWall).getPosY()
	  && (*itWall).getIsBreakable() == false)
	return (-1);
      if (posX == static_cast<int>((*itWall).getPosX())
	  && posY == static_cast<int>((*itWall).getPosY())
	  && (*itWall).getIsBreakable() && (*itWall).isShow())
	{
	  itWall->breakWall(bomberman.getDisplay()->getBonus());
	  if (isPlayer == true)
	    {
	      itPlayer.setScore(1);
	      return (1);
	    }
	  else
	    {
	      itIA.setScore(1);
	      return (1);
	    }
	}
      ++itWall;
    }
  while (itBomb != bomb->end())
    {
      if (posX == static_cast<int>((*itBomb).getPosX())
	  && posY == static_cast<int>((*itBomb).getPosY()))
	{
	  (*itBomb)._willExplose = true;
	  return (1);
	}
      ++itBomb;
    }
  return (0);
}

void        Bomb::checkBombList(Bomberman &bomberman)
{
  ScopedLock                          lock(bomberman.getMutex());
  float                               posX;
  float                               posY;
  bool                                isPlayer = false;

  std::list<Player>                   *player = bomberman.getPlayer();
  std::list<IA>                       *ia = bomberman.getIA();

  std::list<Player>::iterator         itPlayer;
  std::list<IA>::iterator             itIA;
  std::list<Bomb>                     *bomb = bomberman.getBomb();
  int				      side;
  GameEngine *display = bomberman.getDisplay();
  std::vector<AObject*> *listBombGraph = display->getBombs();

  for (std::list<Bomb>::iterator itBomb = bomb->begin(); itBomb != bomb->end(); ++itBomb)
    {
      if ((std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - (*itBomb)._startTime).count() > (*itBomb)._time) || (*itBomb)._willExplose == true)
	{
	  side = 1;
	  addEffect(bomberman.getDisplay()->getExplosions(), itBomb, bomberman);
	  itPlayer = player->begin();
	  itIA = ia->begin();
	  while (itPlayer != player->end() && (*itPlayer).getId() != (*itBomb)._playerId)
	    ++itPlayer;
	  if ((*itPlayer).getId() == (*itBomb)._playerId)
	    isPlayer = true;

	  while (itIA != ia->end() && isPlayer != true
		 && (*itIA).getId() != (*itBomb)._playerId)
	    ++itIA;
	  if ((*itIA).getId() == (*itBomb)._playerId)
	    isPlayer = false;

	  posX = (*itBomb).getPosX() + 1;
	  posY = (*itBomb).getPosY();
 	  while (posX >= (*itBomb).getPosX() - (*itBomb)._range && side != 3)
	    {
	      if ((DestroyObj(bomberman, posX, posY, isPlayer, (*itPlayer), (*itIA)) != 0
		   || posX > (*itBomb).getPosX() + (*itBomb)._range))
		{
		  posX = (*itBomb).getPosX();
		  if (side == 2)
		    side = 3;
		  else
		    side = 2;
		}
	      if (side == 1)
		posX++;
	      else
		posX--;
	    }

	  side = 1;
	  posX = (*itBomb).getPosX();
	  posY = (*itBomb).getPosY() + 1;
	  while (posY >= (*itBomb).getPosY() - (*itBomb)._range && side != 3)
	    {
	      if ((DestroyObj(bomberman, posX, posY, isPlayer, (*itPlayer), (*itIA)) != 0
		   || posY > (*itBomb).getPosY() + (*itBomb)._range))
		{
		  posY = (*itBomb).getPosY();
		  if (side == 2)
		    side = 3;
		  else
		    side = 2;
		}
	      if (side == 1)
		posY++;
	      else
		posY--;
	    }
	  if (isPlayer == true)
	    (*itPlayer).setCapacity(1);
	  else
	    (*itIA).setCapacity(1);
	  itBomb = bomb->erase(itBomb);
    	  ++itBomb;
	  listBombGraph[0][display->getBombNbErase()]->setDrop(false);
	  dynamic_cast<BombGraph*>(listBombGraph[0][display->getBombNbErase()])->reset();
	  display->incrBombNbErase();
	}
    }
}

bool  Bomb::checkWall(int posX, int posY, Bomberman &bomberman)
{
  for (std::list<Wall>::iterator itWall = bomberman.getWall()->begin(); itWall != bomberman.getWall()->end(); itWall++)
  {
    if (posX == (*itWall).getPosX() && posY == (*itWall).getPosY()
      && (*itWall).getIsBreakable() == false)
      return (true);
    if (posX == static_cast<int>((*itWall).getPosX()) && posY == static_cast<int>((*itWall).getPosY())
        && (*itWall).getIsBreakable() && (*itWall).isShow())
        return (true);
  }
  return (false);
}

bool  Bomb::addEffect(std::vector<Explosion*> *explosions, std::list<Bomb>::iterator itBomb, Bomberman &bomberman)
{
  int i;
  size_t j = 0;
  bool   hitwall = false;

  for (i = 0; i <= (*itBomb)._range; i++)
  {
    while (j < explosions->size() && explosions->at(j)->getPrint() == true)
      j++;
    if (j < explosions->size() && hitwall != true)
    {
      explosions->at(j)->Activate();
      explosions->at(j)->translate(glm::vec3((*itBomb).getPosX() + i, 0, (*itBomb).getPosY()));
      if (checkWall((*itBomb).getPosX() + i, (*itBomb).getPosY(), bomberman))
        hitwall = true;
    }
  }
  hitwall = false;
  for (i = 0; i <= (*itBomb)._range; i++)
  {
    while (j < explosions->size() && explosions->at(j)->getPrint() == true)
      j++;
    if (j < explosions->size() && hitwall != true)
    {
      explosions->at(j)->Activate();
      explosions->at(j)->translate(glm::vec3((*itBomb).getPosX() - i, 0, (*itBomb).getPosY()));
      if (checkWall((*itBomb).getPosX() - i, (*itBomb).getPosY(), bomberman))
        hitwall = true;
    }
  }
  hitwall = false;
  for (i = 0; i <= (*itBomb)._range; i++)
  {
    while (j < explosions->size() && explosions->at(j)->getPrint() == true)
      j++;
    if (j < explosions->size() && hitwall != true)
    {
      explosions->at(j)->Activate();
      explosions->at(j)->translate(glm::vec3((*itBomb).getPosX(), 0, (*itBomb).getPosY() + i));
      if (checkWall((*itBomb).getPosX(), (*itBomb).getPosY() + i, bomberman))
        hitwall = true;
    }
  }
  hitwall = false;
  for (i = 0; i <= (*itBomb)._range; i++)
  {
    while (j < explosions->size() && explosions->at(j)->getPrint() == true)
      j++;
    if (j < explosions->size() && hitwall != true)
    {
      explosions->at(j)->Activate();
      explosions->at(j)->translate(glm::vec3((*itBomb).getPosX(), 0, (*itBomb).getPosY() - i));
      if (checkWall((*itBomb).getPosX(), (*itBomb).getPosY() - i, bomberman))
        hitwall = true;
    }
  }
  return (true);
}

int        Bomb::getRange() const
{
  return (_range);
}

int        Bomb::getPlayerId() const
{
  return (_playerId);
}

int        Bomb::getTime() const
{
  return (_time);
}

std::chrono::steady_clock::time_point        Bomb::getStartTime() const
{
  return (_startTime);
}
