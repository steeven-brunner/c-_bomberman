//
// Player.cpp for Bomberman in /home/brunne_s/rendu/cpp_bomberman
//
// Made by Steeven Brunner
// Login   <brunne_s@epitech.net>
//
// Started on  Mon May  4 13:46:05 2015 Steeven Brunner
// Last update Sun Jun 14 16:20:11 2015 Steeven Brunner
//

#include "Bomberman.hh"
#include "Player.hh"

Player::Player(float posX, float posY, std::string name, int id)
  : SoftObject(posX, posY), _name(name), _id(id), _score(0), _range(2), _capacity(1), _isAlive(true), _speed(0)
{
  std::cout << "\n*****Creation*****" << std::endl;
  std::cout << "    _posX : " << _posX << std::endl;
  std::cout << "    _posY : " << _posY << std::endl;
  std::cout << "    _name : " << _name << std::endl;
  std::cout << "    _id   : " << _id << std::endl;
  std::cout << "    _score   : " << _score << std::endl;
  std::cout << "******************\n" << std::endl;
}

Player::Player(float posX, float posY, std::string name, int id, int range, int capacity, int score)
  : SoftObject(posX, posY), _name(name), _id(id), _score(score), _range(range), _capacity(capacity), _isAlive(true), _speed(0)
{
  std::cout << "\n*****Creation*****" << std::endl;
  std::cout << "    _posX : " << _posX << std::endl;
  std::cout << "    _posY : " << _posY << std::endl;
  std::cout << "    _name : " << _name << std::endl;
  std::cout << "    _id   : " << _id << std::endl;
  std::cout << "    _score   : " << _score << std::endl;
  std::cout << "******************\n" << std::endl;
}

Player::~Player()
{
  std::cout << _name << " has been slain !" << std::endl;
}

bool	Player::checkObject(Bomberman & bomberman, float posX, float posY)
{
  std::list<Bonus> * bonus = bomberman.getBonus();
  std::list<Bomb> * bomb = bomberman.getBomb();
  std::list<Wall> * wall = bomberman.getWall();
  std::list<IA> * ia = bomberman.getIA();
  std::list<Player> * player = bomberman.getPlayer();
  std::vector<BonusGraph *> *bonusgraph = bomberman.getDisplay()->getBonus();

  for (std::list<Bonus>::iterator itBonus = bonus->begin(); itBonus != bonus->end(); ++itBonus)
    {
      if (checkCollision(posX, posY, (*itBonus)))
      {
        if ((*itBonus).getType() == MORE_BOMB)
	  setCapacity(1);
	else if ((*itBonus).getType() == FASTER)
	  setSpeed(1);
	else if ((*itBonus).getType() == MORE_RANGE)
	  setRange(1);
        for (size_t i = 0; i < bonusgraph->size(); i++)
        {
          if (bonusgraph->at(i)->getPos().x == itBonus->getPosX() && bonusgraph->at(i)->getPos().z == itBonus->getPosY())
            bonusgraph->at(i)->unActive();
        }
        itBonus = bonus->erase(itBonus);
        return (true);
      }
    }
  for (std::list<Bomb>::iterator itBomb = bomb->begin(); itBomb != bomb->end(); ++itBomb)
    {
      if (checkCollision(posX, posY, (*itBomb)) && !checkCollision(_posX, _posY, (*itBomb)))
      {
        return (false);
      }
    }
  for (std::list<Wall>::iterator itWall = wall->begin(); itWall != wall->end(); ++itWall)
    {
      if (checkCollision(posX, posY, (*itWall)) && itWall->isShow())
        {
          return (false);
        }
    }
  for (std::list<IA>::iterator itIA = ia->begin(); itIA != ia->end(); ++itIA)
    {
      if (checkCollision(posX, posY, (*itIA)) && _id != itIA->getId() && itIA->isAlive())
      {
        return (false);
      }
    }
  for (std::list<Player>::iterator itPlayer = player->begin(); itPlayer != player->end(); ++itPlayer)
    {
      if (checkCollision(posX, posY, (*itPlayer)) && _id != itPlayer->getId() && itPlayer->isAlive())
      {
        return (false);
      }
    }
  return (true);
}

bool  Player::movePlayer(Bomberman & bomberman, float x, float y)
{
  if (checkObject(bomberman, _posX + x, _posY + y))
  {
    _posX += x;
    _posY += y;
    return (true);
  }
  return (false);
}

int     Player::getId() const
{
  return (_id);
}

int     Player::getRange() const
{
  return (_range);
}

int     Player::getCapacity() const
{
  return (_capacity);
}

void    Player::setRange(int range)
{
  if (_range >= 1)
    _range += range;
}

void	Player::setCapacity(int capacity)
{
  _capacity += capacity;
}

void  Player::setAlive(bool alive)
{
  _isAlive = alive;
}

bool  Player::isAlive() const
{
  return (_isAlive);
}

float Player::getSpeed() const
{
  return (_speed);
}

void  Player::setSpeed(float speed)
{
  _speed += speed;
}

void	Player::placeBomb(Bomberman& bomberman)
{
  std::list<Bomb>	*list_bomb = bomberman.getBomb();
  GameEngine *display = bomberman.getDisplay();
  std::vector<AObject*> *listBombGraph = display->getBombs();

  for (std::list<Bomb>::iterator itBomb = list_bomb->begin(); itBomb != list_bomb->end(); ++itBomb)
    {
      if ((*itBomb).getPosX() == static_cast<int>(_posX) && (*itBomb).getPosY() == static_cast<int>(_posY))
      {
	     return ;
      }
    }
  if (_capacity > 0)
    {
      listBombGraph[0][display->getBombNb()]->setDrop(true);
      listBombGraph[0][display->getBombNb()]->translate(glm::vec3(static_cast<int>(_posX) + 0.5, 0, static_cast<int>(_posY) + 0.5));
      display->incrBombNb();
      list_bomb->emplace_back(static_cast<int>(_posX), static_cast<int>(_posY), _range, _id);
      _capacity -= 1;
    }
}

int	Player::getScore() const
{
  return (_score);
}

void	Player::setScore(int score)
{
  _score += score;
}

std::string	Player::getName() const
{
  return (_name);
}

void	  Player::setName(std::string newname)
{
  _name = newname;
}
