//
// Mutex.cpp for cpp_plazza in /home/schler_e/Projet Epitech/C++/cpp_plazza/tp/
//
// Made by Edwin SCHLERET
// Login   <schler_e@epitech.eu>
//
// Started on  Wed Apr 15 11:57:25 2015 Edwin SCHLERET
// Last update Fri Apr 24 17:12:06 2015 Edwin SCHLERET
//

#include "Mutex.hh"

Mutex::Mutex()
{
  if (pthread_mutex_init(&_mutex, NULL) != 0)
    throw ThreadError("Failed pthread_mutex_init");
}

Mutex::~Mutex()
{
  if (pthread_mutex_destroy(&_mutex) != 0)
    throw ThreadError("Failed pthread_mutex_destroy");
}

int 	Mutex::lock()
{
  if ((pthread_mutex_lock(&_mutex)) != 0)
    throw ThreadError("Failed pthread_mutex_lock");
  return (0);
}

int 	Mutex::unlock()
{
  if ((pthread_mutex_unlock(&_mutex)) != 0)
    throw ThreadError("Failed pthread_mutex_unlock");
  return (0);
}

bool	Mutex::trylock()
{
  if ((pthread_mutex_trylock(&_mutex)) == 0)
    return true;
  return false;
}

pthread_mutex_t   *Mutex::getMutex()
{
  return (&_mutex);
}
