//
// ACharacter.cpp for ACharacter in /home/dayrie_l/rendu/cpp_bomberman/graphic
//
// Made by Lucas Dayries
// Login   <dayrie_l@epitech.net>
//
// Started on  Mon May 11 14:15:45 2015 Lucas Dayries
// Last update Sat Jun 13 13:07:28 2015 Steeven Brunner
//

#include "ACharacter.hpp"

ACharacter::ACharacter()
  : _position(-0.5, -0.5, -0.5), _rotation(0, 0, 0), _scale (0.0019, 0.0019, 0.0019)
{
}

ACharacter::~ACharacter()
{
}

bool	ACharacter::initialize()
{
  return (true);
}

void	ACharacter::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
}

void	ACharacter::translate(glm::vec3 const &v)
{
  _position += v;
}

void	ACharacter::rotate(glm::vec3 const& axis, float angle)
{
  _rotation = axis * angle;
}

void	ACharacter::scale(glm::vec3 const& scale)
{
  _scale *= scale;
}

glm::mat4	ACharacter::getTransformation()
{
  glm::mat4 transform(1);
  transform = glm::translate(transform, _position);
  transform = glm::scale(transform, _scale);
  transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
  transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
  transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
  return (transform);
}

glm::vec3 ACharacter::getPosition()
{
  return (_position);
}
